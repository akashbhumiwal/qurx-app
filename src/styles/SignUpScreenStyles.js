import { StyleSheet } from 'react-native';
import AppColors from '../themeVariables/AppColors';
import { Dimensions } from "react-native";
const baseWidth = 375;
const baseHeight = 667;
const { width, height } = Dimensions.get("window");
var deviceHeight = Dimensions.get("window").height;
var deviceWidth = Dimensions.get("window").width;
const scaleWidth = width / baseWidth;
const scaleHeight = height / baseHeight;
const scale = Math.min(scaleWidth, scaleHeight);
export const scaledSize = size => Math.ceil(size * scale);
const SignUpScreenStyles = StyleSheet.create({
    parentViewStyles: { flex: 1, backgroundColor: "white", justifyContent: "center", justifyContent: "center", marginBottom: 45 },
    headingStyles: { color: AppColors.theme_text_color, fontSize: 18, marginTop: 20, fontWeight: "600" },
    imgStyles: { width: 15, height: 15 },
    phoneNoTextStyles: { color: AppColors.theme_text_color, fontSize: 17, marginTop: 5 },
    cancelImgStyles: { width: 25, height: 25 },
    container: { justifyContent: "center", justifyContent: "center" },
    submitOtp: { marginTop: 28, backgroundColor: '#1ec97f', borderColor: '#45db5e' },
    submitOtpText: { fontWeight: "500", fontSize: scaledSize(17), color: '#fff', textAlign: 'center', },
    btnContainerStyles: { flexDirection: "row", justifyContent: "space-between", paddingHorizontal: 10 },
    resendOtpTextStyles: { color: AppColors.theme_blue_color, fontSize: 15, marginTop: 20, textDecorationLine: 'underline' },
    notRecievedTextStyles: { color: AppColors.theme_text_color, fontSize: 15, marginTop: 20, },
    confirmBtnStyles: { flexDirection: "row", alignItems: "center", justifyContent: "space-between", padding: 15 },
    emailTextStyles: { color: AppColors.theme_text_color, fontSize: 14, marginTop: 15 },
    input: { color: AppColors.theme_text_color, borderColor: AppColors.gray_light, fontSize: 19, marginTop: 10, borderWidth: 0.25, height: 50, backgroundColor: '#ffffff', paddingLeft: 15, paddingRight: 15 },
    submit: { marginTop: 25, paddingTop: 5, paddingVertical: 15, backgroundColor: '#1ec97f', borderColor: '#45db5e' },
    submitText: { fontWeight: "600", marginTop: scaledSize(5), fontSize: scaledSize(17), color: '#fff', textAlign: 'center', },
    orText: { marginBottom: -15, fontWeight: "600", marginTop: scaledSize(15), fontSize: scaledSize(17), color: AppColors.theme_text_light_grey_color, textAlign: 'center', },
    popUpContainer: { backgroundColor: "rgba(52, 52, 52, 0.8)", justifyContent: "center", alignItems: "center", },
});

export default SignUpScreenStyles;