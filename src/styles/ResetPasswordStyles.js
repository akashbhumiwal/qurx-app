
import { StyleSheet } from 'react-native';
import AppColors from '../themeVariables/AppColors';
import { Dimensions } from "react-native";
const baseWidth = 375;
const baseHeight = 667;
const { width, height } = Dimensions.get("window");
var deviceHeight = Dimensions.get("window").height;
var deviceWidth = Dimensions.get("window").width;
const scaleWidth = width / baseWidth;
const scaleHeight = height / baseHeight;
const scale = Math.min(scaleWidth, scaleHeight);
export const scaledSize = size => Math.ceil(size * scale);
const ResetPasswordStyles = StyleSheet.create({
    parentViewStyles: { flex: 1, justifyContent: "center", justifyContent: "center" },
    popUpheadingStyles: { color: AppColors.gray_dark, fontSize: 19, marginTop: 20, fontWeight: "600" },
    textStyles: { color: AppColors.theme_text_color, fontSize: 17, marginTop: 20, fontWeight: "600" },
    imgStyles: { width: 15, height: 15 },
    cancelImgStyles: { width: 25, height: 25 },
    resetPassinput: { color: AppColors.theme_text_color, backgroundColor: '#ffffff', fontSize: 14, marginTop: 12, borderWidth: 0.5, height: 40, paddingLeft: 15, paddingRight: 15, borderColor: AppColors.gray_light },
    submit: { marginTop: 18, backgroundColor: '#1ec97f', borderColor: '#45db5e' },
    confirmText: { fontWeight: "bold", fontSize: scaledSize(16), color: '#fff', textAlign: 'center', },
    phoneNoTextStyles: { color: AppColors.theme_text_color, fontSize: 17, marginTop: 5 },
    btnContainerStyles: { flexDirection: "row", justifyContent: "space-between", },
    greyTextStyles: { color: AppColors.gray_light, fontSize: 18, fontWeight: "600" },
    resendOtpTextStyles: { color: AppColors.theme_blue_color, fontSize: 17, marginTop: 20, textDecorationLine: 'underline' },
    notRecievedTextStyles: { color: AppColors.theme_text_color, fontSize: 17, marginTop: 20, },
    confirmBtnStyles: { flexDirection: "row", alignItems: "center", justifyContent: "space-between", padding: 15 },
});

export default ResetPasswordStyles;