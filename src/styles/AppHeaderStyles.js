import { StyleSheet } from "react-native";
import AppDimentions from "../themeVariables/AppDimentions";
import QurxAppColors from "../themeVariables/QurxAppColors";
const headerStyle = StyleSheet.create({
  parent: {
    backgroundColor: QurxAppColors.headerColor,
    borderBottomWidth: 0,
    paddingLeft: 10,
    marginTop: AppDimentions.current_platform ? 0 : 0
  },

  parentTwo: {
    backgroundColor: QurxAppColors.headerColor,
    borderBottomWidth: 0
  },

  leftPart: {
    flex: 1,
    paddingLeft: 10
  },

  // this style is using by menu and switch icons
  menuButton: {
    height: 20,
    width: 20,
    margin: 5,
    paddingHorizontal: 15
  },

  bodyPart: {
    flex: 3
  },

  rightPart: {
    flex: 1
  },

  textStyle: {
    fontWeight: "600",
    fontSize: 22,
    color: "white",
    alignSelf: "center"
  },

  rightActionText: {
    fontSize: 13,
    color: "white",
    // fontFamily: "lato-bold",
    alignSelf: "center",
    padding: 5
  }
});

export default headerStyle;
