import { StyleSheet } from 'react-native';
import AppColors from '../themeVariables/AppColors';
import { Dimensions } from "react-native";
const baseWidth = 375;
const baseHeight = 667;
const { width, height } = Dimensions.get("window");
const scaleWidth = width / baseWidth;
const scaleHeight = height / baseHeight;
const scale = Math.min(scaleWidth, scaleHeight);
export const scaledSize = size => Math.ceil(size * scale);
const LoginScreenStyles = StyleSheet.create({
    container: { flex: 1, backgroundColor: "white", justifyContent: "center", justifyContent: "center", },
    cancelImgStyles: { width: 25, height: 25 },
    headingStyles: { color: AppColors.theme_text_color, fontSize: 14, marginTop: 20, fontWeight: "600" },
    popUpContainer: { backgroundColor: "rgba(52, 52, 52, 0.8)", alignItems: "center", },
    popUpheadingStyles: { color: AppColors.gray_dark, fontSize: 19, marginTop: 20, fontWeight: "600" },
    searchBtnStyles: { flexDirection: "row", paddingHorizontal: 10 },
    searchParentViewStyles: { flexDirection: "row", borderRadius: 12, backgroundColor: "white" },
    searchImgStyles: { marginLeft: 10, height: 20, width: 20, alignSelf: "center" },
    searchTextStyles: { paddingHorizontal: 10, fontSize: 18, padding: 2, },
    greyTextStyles: { color: AppColors.gray_light, fontSize: 15, fontWeight: "600" },
    imgStyles: { width: 15, height: 15 },
    confirmText: { fontWeight: "bold", fontSize: scaledSize(16), color: '#fff', textAlign: 'center', },
    emailTextStyles: { color: AppColors.theme_text_color, fontSize: 17, marginTop: 40 },
    popUpconfirmBtnStyles: { flexDirection: "row", alignItems: "center" },
    confirmBtnStyles: { flexDirection: "row", alignItems: "center", justifyContent: "space-between", padding: 15 },
    popUpinput: { color: AppColors.theme_text_color, backgroundColor: '#ffffff', fontSize: 13, marginTop: 15, borderWidth: 0.5, height: 40, paddingLeft: 15, paddingRight: 15, borderColor: AppColors.gray_light },
    input: { color: AppColors.theme_text_color, borderColor: AppColors.gray_light, fontSize: 19, marginTop: 10, borderWidth: 0.25, height: 55, backgroundColor: '#ffffff', paddingLeft: 15, paddingRight: 15 },
    submit: { marginTop: 28, paddingTop: 5, paddingVertical: 15, backgroundColor: '#1ec97f', borderColor: '#45db5e' },
    popUpsubmit: { padding: 5, backgroundColor: '#1ec97f', borderColor: '#45db5e' },
    submitText: { fontWeight: "600", marginTop: scaledSize(5), fontSize: scaledSize(16), color: '#fff', textAlign: 'center', },
    orText: { marginBottom: -15, fontWeight: "600", marginTop: scaledSize(15), fontSize: scaledSize(17), color: AppColors.theme_text_light_grey_color, textAlign: 'center', },
});

export default LoginScreenStyles;