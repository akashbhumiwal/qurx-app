import { StyleSheet } from 'react-native';
import AppColors from '../themeVariables/AppColors';
import { Dimensions } from "react-native";
const baseWidth = 375;
const baseHeight = 667;
const { width, height } = Dimensions.get("window");
const scaleWidth = width / baseWidth;
const scaleHeight = height / baseHeight;
const scale = Math.min(scaleWidth, scaleHeight);
export const scaledSize = size => Math.ceil(size * scale);
const HomeScreenStyles = StyleSheet.create({
    // parentView: { justifyContent: "center", marginTop: 20 },
    parentView: { justifyContent: "center" },
    // gridCellParentStyles: { paddingVertical: 4, paddingHorizontal: 10 },
    headingStyles: { marginBottom: 10, color: AppColors.theme_text_color, fontSize: 18, paddingHorizontal: 10, margin: 5, marginTop: 20, fontWeight: "600" },
    searchBtnStyles: { flexDirection: "row", paddingHorizontal: 10 },
    searchParentViewStyles: { borderRadius: 10, height: 40, width: "95%", marginTop: 5, justifyContent: "center", backgroundColor: "white", marginLeft: 8 },
    searchImgStyles: { marginLeft: 10, height: 20, width: 20, alignSelf: "center" },
    searchTextStyles: { color: AppColors.theme_text_light_grey_color, paddingHorizontal: 10, fontSize: 18, padding: 2, },
    container: { backgroundColor: "#fff" },
    parentCardView: { borderRadius: 10, borderColor: AppColors.theme_text_light_grey_color, borderWidth: .6, width: 168, height: 120, backgroundColor: "white" },
    childCardView: { marginLeft: 30, marginTop: 20 },
    text: { fontSize: 17, color: "black", },
    textInputStyles: { marginLeft: 10, height: 40, flex: 1, backgroundColor: "transparent" },
    imgStyles: { marginBottom: 15, height: 30, width: 35 },
});

export default HomeScreenStyles;