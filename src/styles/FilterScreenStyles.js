import { StyleSheet } from 'react-native';
import AppColors from '../themeVariables/AppColors';
import { Dimensions } from "react-native";
const baseWidth = 375;
const baseHeight = 667;
const { width, height } = Dimensions.get("window");
var deviceHeight = Dimensions.get("window").height;
var deviceWidth = Dimensions.get("window").width;
const scaleWidth = width / baseWidth;
const scaleHeight = height / baseHeight;
const scale = Math.min(scaleWidth, scaleHeight);
export const scaledSize = size => Math.ceil(size * scale);
const FilterScreenStyles = StyleSheet.create({

    // parentView: { justifyContent: "center", },
    parentView: { justifyContent: "center", backgroundColor: AppColors.white },
    childView: { paddingHorizontal: 10 },
    blueTextStyles: { color: AppColors.blue_dark, fontSize: 20, margin: 5, fontWeight: "600" },
    greyTextStyles: { color: AppColors.gray_light, fontSize: 18, fontWeight: "600" },
    checkBoxTextStyles: { color: AppColors.gray_light, fontSize: 18, fontWeight: "600", marginHorizontal: 5 },
    text: { fontSize: 17, color: "black" },
    TextStyle: { fontSize: 20, color: '#000', textAlign: 'left' },
    gridCellParentStyles: { flex: 1, height: 140, },
    filterParentViewStyles: { flexDirection: "row", marginHorizontal: 20 },
    filterBtnStyles: { flexDirection: "row", flex: 1, alignItems: "center" },
    cellParentStyles: { flex: 1, backgroundColor: "white", marginTop: 5 },
    searchImgStyles: { marginLeft: 10, height: 20, width: 20, alignSelf: "center" },
    headingStyles: { color: AppColors.theme_text_color, fontSize: 18, margin: 5, fontWeight: "600" },
    parentCardView: { borderRadius: 10, borderColor: AppColors.theme_text_light_grey_color, borderWidth: .6, width: "100%", height: 170 },
    childCardView: { flex: 1, flexDirection: "row", marginLeft: 20, marginTop: 10, borderRadius: 10, borderColor: AppColors.theme_text_light_grey_color, borderWidth: .6, },
    submit: { marginHorizontal: 20, marginTop: 15, paddingVertical: 15, backgroundColor: '#1ec97f', borderColor: '#45db5e' },
    submitText: { fontWeight: "600", marginTop: scaledSize(5), fontSize: scaledSize(17), color: '#fff', textAlign: 'center', },
});

export default FilterScreenStyles;