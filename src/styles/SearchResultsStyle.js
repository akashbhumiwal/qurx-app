import { StyleSheet } from 'react-native';
import AppColors from '../themeVariables/AppColors';
import { Dimensions } from "react-native";
const baseWidth = 375;
const baseHeight = 667;
const { width, height } = Dimensions.get("window");
var deviceHeight = Dimensions.get("window").height;
var deviceWidth = Dimensions.get("window").width;
const scaleWidth = width / baseWidth;
const scaleHeight = height / baseHeight;
const scale = Math.min(scaleWidth, scaleHeight);
export const scaledSize = size => Math.ceil(size * scale);
const SearchResultsStyle = StyleSheet.create({
    container: { marginVertical: 5 },
    parentView: { flex: 1, justifyContent: "center", backgroundColor: AppColors.white },
    parentMapView: { flex: 1, justifyContent: "center", backgroundColor: AppColors.white },
    filterParentViewStyles: { flexDirection: "row", marginHorizontal: 20 },
    filterBtnStyles: { flexDirection: "row", flex: 1, alignItems: "center" },
    cellParentStyles: { marginTop: 5, backgroundColor: AppColors.white, paddingBottom: 15 },
    searchImgStyles: { marginLeft: 10, height: 20, width: 20, alignSelf: "center" },
    parentCardView: { borderColor: AppColors.theme_text_light_grey_color, },
    childCardView: { flex: 1, flexDirection: "row", marginLeft: 20, marginTop: 10 },
    imgStyles: { width: 50, height: 50, marginBottom: 15 },
    verifiedImgStyles: { width: 22, height: 22, marginTop: 5, marginLeft: 3 },
    callImgStyles: { width: 15, height: 15 },
    nameTextStyles: { color: AppColors.blue_dark, fontSize: 18, margin: 5, fontWeight: "600" },
    feeTextStyles: { fontSize: 13, color: AppColors.theme_text_color, fontWeight: "600", marginVertical: 8 },
    adressTextStyles: { fontSize: 14, color: AppColors.theme_text_color, marginBottom: 8 },
    filterTextStyles: { fontSize: 14, marginHorizontal: 5, color: AppColors.theme_text_color },
    professionTextStyles: { marginHorizontal: 5, fontSize: 14, color: AppColors.theme_text_color, marginBottom: 4 },
    roundImgStyles: { width: 73, height: 73, borderRadius: 73 / 2, borderWidth: 1, borderColor: AppColors.black },
    submit: { backgroundColor: '#1ec97f', justifyContent: "center" },
    submitCall: { marginLeft: 5, alignItems: "center", flexDirection: "row", backgroundColor: '#1ec97f', paddingHorizontal: 15, justifyContent: "center", fontWeight: "600" },
    buttonText: { paddingHorizontal: 5, paddingVertical: 10, fontWeight: "600", fontSize: scaledSize(10), color: '#fff', textAlign: 'center', },
});

export default SearchResultsStyle;

// padding: 10, borderColor: "green", borderWidth: 5,