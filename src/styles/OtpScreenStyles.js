import { StyleSheet } from 'react-native';
import AppColors from '../themeVariables/AppColors';
import { Dimensions } from "react-native";
const baseWidth = 375;
const baseHeight = 667;
const { width, height } = Dimensions.get("window");
var deviceHeight = Dimensions.get("window").height;
var deviceWidth = Dimensions.get("window").width;
const scaleWidth = width / baseWidth;
const scaleHeight = height / baseHeight;
const scale = Math.min(scaleWidth, scaleHeight);
export const scaledSize = size => Math.ceil(size * scale);
const OtpScreenStyles = StyleSheet.create({
    parentViewStyles: { justifyContent: "center", justifyContent: "center" },
    headingStyles: { color: AppColors.theme_text_color, fontSize: 16, marginTop: 20, fontWeight: "800" },
    imgStyles: { width: 15, height: 15 },
    input: { backgroundColor: '#ffffff', fontSize: 18, marginTop: 15, borderWidth: 0.5, height: 60, paddingLeft: 15, paddingRight: 15, borderColor: AppColors.gray_light },
    submit: { marginTop: 28, backgroundColor: '#1ec97f', borderColor: '#45db5e' },
    submitText: { fontWeight: "500", fontSize: scaledSize(17), color: '#fff', textAlign: 'center', },
    phoneNoTextStyles: { color: AppColors.theme_text_color, fontSize: 17, marginTop: 5 },
    btnContainerStyles: { flexDirection: "row", justifyContent: "space-between", },
    resendOtpTextStyles: { color: AppColors.theme_blue_color, fontSize: 17, marginTop: 20, textDecorationLine: 'underline' },
    notRecievedTextStyles: { color: AppColors.theme_text_color, fontSize: 17, marginTop: 20, },
    confirmBtnStyles: { flexDirection: "row", alignItems: "center", justifyContent: "space-between", padding: 15 },
});

export default OtpScreenStyles;