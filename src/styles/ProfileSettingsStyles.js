
import { StyleSheet } from 'react-native';
import AppColors from '../themeVariables/AppColors';
import { Dimensions } from "react-native";
const baseWidth = 375;
const baseHeight = 667;
const { width, height } = Dimensions.get("window");
var deviceHeight = Dimensions.get("window").height;
var deviceWidth = Dimensions.get("window").width;
const scaleWidth = width / baseWidth;
const scaleHeight = height / baseHeight;
const scale = Math.min(scaleWidth, scaleHeight);
export const scaledSize = size => Math.ceil(size * scale);
const ProfileSettingsStyles = StyleSheet.create({
    parentViewStyles: { justifyContent: "center", justifyContent: "center" },
    headingStyles: { color: AppColors.theme_text_color, fontSize: 15, marginTop: 20, fontWeight: 'bold' },
    imgStyles: { width: 15, height: 15 },
    camImgStyles: { width: 25, height: 25, marginTop: -38, marginLeft: 70 },
    profileDummyImageStyles: { borderWidth: 3, borderColor: AppColors.white, marginTop: -125, marginBottom: 20, width: 100, height: 100, borderRadius: 100 / 2 },
    input: { color: AppColors.theme_text_color, backgroundColor: '#ffffff', fontSize: 14, marginTop: 15, borderWidth: 0.5, height: 40, paddingLeft: 15, paddingRight: 15, borderColor: AppColors.gray_light },
    submit: { marginTop: 28, backgroundColor: '#1ec97f', borderColor: '#45db5e' },
    confirmText: { fontWeight: "bold", fontSize: scaledSize(17), color: '#fff', textAlign: 'center', },
    phoneNoTextStyles: { color: AppColors.theme_text_color, fontSize: 17, marginTop: 5 },
    btnContainerStyles: { flexDirection: "row", justifyContent: "space-between", },
    resendOtpTextStyles: { color: AppColors.theme_blue_color, fontSize: 17, marginTop: 20, textDecorationLine: 'underline' },
    notRecievedTextStyles: { color: AppColors.theme_text_color, fontSize: 17, marginTop: 20, },
    confirmBtnStyles: { flexDirection: "row", alignItems: "center", justifyContent: "space-between", padding: 15 },
});

export default ProfileSettingsStyles;