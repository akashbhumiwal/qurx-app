import AppDimentions from "../themeVariables/AppDimentions";
import { Alert, NetInfo, } from "react-native";
import AsyncStorage from '@react-native-community/async-storage';
export default class Utility {
  static myInstance = null;
  current_latitude = null;
  current_longitude = null;
  error = "";
  message_threshold_limit = 50;
  isInternetConneted = false;
  isAlertShown = false;
  tour_start_time = null;
  tour_elapsed_time = 0;
  tour_elapsed_meter = 0;
  tour_elapsed_mile = 0;
  tour_started = false;
  previous_position = null;
  watch_location_for_tour = false;
  driver_id = -1;
  location_counter = 0;
  LocationPayload = [];
  locationEventListener = null;
  networkChangeDetected = null;
  /**
   * @returns {Utility}
   */
  static getInstance() {
    if (this.myInstance == null) {
      this.myInstance = new Utility();
    }

    return this.myInstance;
  }

  storeQrexData = async (key_to_be_paired, data_to_save) => {
    try {
      await AsyncStorage.setItem(key_to_be_paired, data_to_save)
    } catch (e) {
      console.log("error while storing", e);
    }
  }
  ///////////STORING  USER  LOGIN DATA/////////////
  storeLoginUserData = async (key_to_be_paired, data_to_save) => {
    try {
      await AsyncStorage.setItem(key_to_be_paired, data_to_save)
    } catch (e) {
      console.log("error while storing", e);
    }
  }


  ///////////STORING  USER  SIGN UP DATA/////////////
  storeSignUpData = async (key_to_be_paired, data_to_save) => {
    try {
      await AsyncStorage.setItem(key_to_be_paired, data_to_save)
    } catch (e) {
      console.log("error while storing", e);
    }
  }

  getQrexData = async (key_to_be_paired) => {
    try {
      const value = await AsyncStorage.getItem(key_to_be_paired)
      if (value !== null) {
        // value previously stored
      }
    } catch (e) {
      console.log("error while storing", e);
      // error reading value
    }
  }

  /**
     * initalize network connections.
     */
  startWatchingNetwork() {
    NetInfo.getConnectionInfo().then(() => { });
    function handleFirstConnectivityChange(connectionInfo) {
      if (connectionInfo.type == "wifi") {
        Utility.getInstance().isInternetConneted = true;
      } else if (connectionInfo.effectiveType == "4g") {
        Utility.getInstance().isInternetConneted = true;
      } else if (connectionInfo.effectiveType == "3g") {
        Utility.getInstance().isInternetConneted = true;
      } else if (connectionInfo.effectiveType == "2g") {
        Utility.getInstance().isInternetConneted = true;
      } else {
        Utility.getInstance().isInternetConneted = false;
      }
    }
    NetInfo.addEventListener("connectionChange", handleFirstConnectivityChange);
  }


  startWatchingNetworkChange() {
    return new Promise((resolve) => {
      let isConnected = false;
      NetInfo.getConnectionInfo().then(() => { });
      function handleFirstConnectivityChange(connectionInfo) {
        if (connectionInfo.type == "wifi") {
          isConnected = true;
        } else if (connectionInfo.effectiveType == "4g") {
          isConnected = true;
        } else if (connectionInfo.effectiveType == "3g") {
          isConnected = true;
        } else if (connectionInfo.effectiveType == "2g") {
          isConnected = true;
        } else {
          isConnected = false;
        }

        resolve({ isConnected: isConnected });
        if (Utility.getInstance().getDispatch() != null)
          Utility.getInstance().getDispatch()({ isConnected: isConnected });

        Utility.getInstance().isInternetConneted = isConnected;
      }
      NetInfo.addEventListener(
        "connectionChange",
        handleFirstConnectivityChange
      );
    });
  }

  disableNetworkConnection() {
    const dispatchConnected = isConnected => this.setIsConnected(isConnected);
    NetInfo.removeEventListener("change", dispatchConnected);
  }

  /**
   * retrurns connection value
   */
  isNetworkConnected() {
    if (AppDimentions.current_platform) return true;
    else return Utility.getInstance().isInternetConneted;
  }

  /**
   * sets internet connection value in global variable
   * @param {connetion value} isConnected
   */
  setIsConnected(isConnected) {
    Utility.getInstance().isInternetConneted = isConnected;
  }

  /**
   * shows no network dialong
   */
  showNoNetworkDialog() {
    if (!Utility.getInstance().isAlertShown) {
      Utility.getInstance().isAlertShown = true;

      Alert.alert(
        "No Internet Connection",
        "Internet connection is required in order to updated trip status",
        [
          {
            text: "Cancel",
            onPress: this.cancelPress.bind(this),
            style: "cancel"
          },
          { text: "Confirm", onPress: this.openSettingsPage.bind(this) }
        ],
        { cancelable: true }
      );
    }
  }

  /**
   * shows access token mismatch dialong
   */

  handleTokenExpireLogout(navigation) {
    Alert.alert("Access Token Mismatch please login again");
    setTimeout(() => {
      navigation.navigate("Login");
    }, 2000);
  }

  cancelPress() {
    Utility.getInstance().isAlertShown = false;
  }
}
export function isEmptyField(item_to_check) {
  if (
    item_to_check == null ||
    item_to_check == undefined ||
    item_to_check == "" ||
    item_to_check == "null"
  )
    return true;
  return false;
}

export function isEmpty(obj) {
  for (var key in obj) {
    if (obj.hasOwnProperty(key)) return false;
  }
  return true;
}