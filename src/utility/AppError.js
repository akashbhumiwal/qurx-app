import StringConstants from "../utility/StringConstants";
export default {
  NETWORK_ERROR_CODE: 503,
  TOKEN_ERROR_CODE: 504,

  getNetworkError() {
    return {
      status: this.NETWORK_ERROR_CODE,
      error: StringConstants.NETWORK_ERROR_MESSAGE
    };
  },

  getTokenError() {
    return {
      status: this.TOKEN_ERROR_CODE,
      error: StringConstants.TOKEN_ERROR_MESSAGE
    };
  },

  getJSONError() {
    return {
      status: this.TOKEN_ERROR_CODE,
      error: StringConstants.JSON_PARSE_ERROR_MESSAGE
    };
  },

  getResponseCodeErrorMessage(code) {
    return "Status Code " + code;
  },

  parseError(error) {
    if (
      error != undefined &&
      error.error != undefined &&
      error.error.response != undefined &&
      error.error.response.data != undefined
    ) {
      return {
        status: error.status,
        title: error.error.response.data.title,
        message: error.error.response.data.description
      };
    } else if (
      error.status != undefined &&
      error.status == this.NETWORK_ERROR_CODE
    ) {
      return {
        status: error.status,
        title: StringConstants.NETWORK_ERROR_NAME,
        message: error.error
      };
    } else {
      return {
        status: error,
        title: error,
        message: error
      };
    }
  }
};

export function parseError(error) {
  if (
    error != undefined &&
    error.error != undefined &&
    error.error.response != undefined &&
    error.error.response.data != undefined
  ) {
    return {
      status: error.status != undefined ? error.status : "",
      title:
        error.error.response.data.title != undefined
          ? error.error.response.data.title
          : "",
      message:
        error.error.response.data.description != undefined
          ? error.error.response.data.description
          : ""
    };
  } else if (
    error.status != undefined &&
    error.status == this.NETWORK_ERROR_CODE
  ) {
    return {
      status: error.status,
      title: StringConstants.NETWORK_ERROR_NAME,
      message: error.error
    };
  } else {
    return {
      status: error,
      title: error,
      message: error
    };
  }
}
