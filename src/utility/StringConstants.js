const GEOLOC_MESS = "Access to your location is required in order to start a trip, Please enable it from settings.";
export default {
  NETWORK_ERROR_NAME: "Network Error",
  NETWORK_ERROR_MESSAGE: "You are not connected to internet",
  TOKEN_ERROR_MESSAGE: "Unable to find any token for this request",
  JSON_PARSE_ERROR_MESSAGE: "Unable to parse the JSON",
  IS_ADMIN: "IS_ADMIN",
  GET_GARRAGE: "GET_GARRAGE",
  GET_AMENITIES: "GET_AMENITIES",
  GET_USER_DETAIL: "GET_USER_DETAIL",
  SOLD_OUT: "Sold Out",
  QREX_ROLE_CONTEXT: "qrexRole",
  SIGN_UP_DATA: "signUpData",
  LOGIN_USER_DATA: "loginData",

  ////////RESERVATION ACTIONS CONSTANTS/////////Send Driver Info
  RESEND_RECEIPT: "Resend Receipt",
  //Add Complaints constants//
  ADD_COMPLAINT_TITLE: "Create Complaint",
  ADD_COMPLAINT_REASON: "Reason",
  ADD_COMPLAINT_DESC: "Enter in the description of the complaint and a ticket will be created",

  //CANCEL TRIP CONSTANTS//
  CANCEL_TRIP_TITLE: "Start Cancellation",
  CANCEL_TRIP_REASON: "Reason",
  CANCEL_TRIP_DESC: "Clicking Start will send an email to the customer to collect more information",


  ADD_COMPLAINT: "Add Complaint",
  CANCEL_TRIP: "Cancel Trip",
  SEND_DRIVER_INFO: "Send Driver Info",

  // GEO_LOCATION_PERMISSION: "Geo-location Permission",
  GEO_LOCATION_PERMISSION: "Your location is required",
  GEO_LOCATION_MESSAGE: GEOLOC_MESS,
  GEO_LOCATION_ERROR_MESSAGE: GEOLOC_MESS,


  ///LOGIN  MESSAGES///
  ENTER_USERNAME_MESSAGE: "Please enter Username/Email",
  ENTER_VALID_PASSWORD_MESSAGE: "Please enter a valid password",
  SELECT_CHOICE_MESSAGE: "Please select a choice",
  LOGIN_SUCCESSFUL_MESSAGE: "Login successful",
  ERROR_MESSAGE: "Error",
  QURX_APP_ALERT: "Qurx App",

  ///SIGNUP  MESSAGES///
  ENTER_FIRSTNAME_MESSAGE: "Please enter First name",
  ENTER_LASTNAME_MESSAGE: "Please enter Last name",
  ENTER_VALID_EMAIL_MESSAGE: "Please enter a valid email",
  ENTER_MOBILENUM_MESSAGE: "Please enter your Mobile number",
  ENTER_VALID_DIGITS_PASSWORD_MESSAGE: "Please enter a valid password a minimum of 8 digits",
  REENTER_PASSWORD_MESSAGE: "Please re enter the password ",
  PASSWORD_MISMATCH_MESSAGE: "Passwords does not match !",
  ENTER_OTP_MESSAGE: "Please enter Otp",

  ///ACCOUNT SETTINGS  MESSAGES///
  ENTER_FIRSTNAME_MESSAGE: "Account Settings Work in Progress",


};
