import AppConstants from "../utility/AppConstants";
export default {
  log(message) {
    if (AppConstants.SHOULD_LOG) console.log(message);
  }
};
