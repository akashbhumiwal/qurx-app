// specify the enviroment variable to build the app for.

var appEnvironmentToBuild = "dev"; //  'dev' or 'edge' or 'development'  or 'production'

const getBuildEnviroment = () => {
  if (appEnvironmentToBuild == "dev") {
    return {
      BASE_URL: "https://api.qurx.in", //'https://api.app.coachrail.com',//'https://api.qa-mo.coachrail-qa.com',
      // SOCKET_BASE_URL: "https://tracking.dev.coachrail.com", //"http://18.218.68.126:3000", //'https://tracking.app.coachrail.com',
      Referer_URL: "https://api.qurx.in",
      // GOOGLE_MAPS_APIKEY: "AIzaSyBGz9rVf62vyvXReFJxZ648nhQi2dDzgn4", //"AIzaSyB1AMY66GVJ3VhvSGd4vIenocg6UHwOpfE",
      // SOCKET_SERVER_TOKEN: "123456",
      // IMAGE_BASE_URL: "https://dev.coachrail.com"
    };
    // return enviroment_object;
  } else if (appEnvironmentToBuild == "production") {
    return {
      BASE_URL: "https://api.qurx.in", //'https://api.app.coachrail.com',//'https://api.qa-mo.coachrail-qa.com',
      // SOCKET_BASE_URL: "https://tracking.app.coachrail.com", //"http://18.218.68.126:3000", //'https://tracking.app.coachrail.com',
      Referer_URL: "https://api.qurx.in",
      // GOOGLE_MAPS_APIKEY: "AIzaSyB1AMY66GVJ3VhvSGd4vIenocg6UHwOpfE",
      // SOCKET_SERVER_TOKEN: "123456",
      // IMAGE_BASE_URL: "https://app.coachrail.com"
    };
    // return enviroment_object;
  } else {
    return {
      BASE_URL: "https://api.qurx.in", //'https://api.app.coachrail.com',//'https://api.qa-mo.coachrail-qa.com',
      // SOCKET_BASE_URL: "http://18.218.68.126:3000", //'https://tracking.app.coachrail.com',
      Referer_URL: "https://api.qurx.in",
      // GOOGLE_MAPS_APIKEY: "AIzaSyB1AMY66GVJ3VhvSGd4vIenocg6UHwOpfE",
      // SOCKET_SERVER_TOKEN: "123456",
      // IMAGE_BASE_URL: "https://dev.coachrail.com"
    };
  }
};
export default {
  ANDROID: "android",
  IOS: "iOS",
  SHOULD_LOG: appEnvironmentToBuild === "production" ? false : true,
  IS_SOCKET_SERVER_ENABLED: false,
  IS_DEV: appEnvironmentToBuild !== "production" ? true : false,
  BASE_URL: getBuildEnviroment().BASE_URL,
  Referer_URL: getBuildEnviroment().Referer_URL,
  // GOOGLE_MAPS_APIKEY: getBuildEnviroment().GOOGLE_MAPS_APIKEY,
  // SOCKET_SERVER_TOKEN: getBuildEnviroment().SOCKET_SERVER_TOKEN,
  // IMAGE_BASE_URL: getBuildEnviroment().IMAGE_BASE_URL,
  VERSION: "1.0",
};

