import { Button, Container, Content, Text } from "native-base";
import React, { Component } from "react";
import AppColors from "../themeVariables/AppColors"
import { Alert, Animated, Image, Keyboard, View, StatusBar, Platform, Dimensions } from "react-native";
const downArrow = require("../PagesImages/down-arrow.png");
const DEVICE_WIDTH = Dimensions.get("window").width;
const DEVICE_HEIGHT = Dimensions.get("window").height;
const platform = Platform.OS;
const isAndroid = platform !== "ios";
const HomeScreenHeader = ({ leftText, rightText, centerText, }) => {
    return (
        // <View style={{ flex: 1, flexDirection: 'row', justifyContent: "space-between", marginBottom: 50, marginTop: 50 }}>
        <View style={{ flex: 1, flexDirection: 'row', justifyContent: "space-between", marginBottom: 50, marginTop: isAndroid ? 25 : 50 }}>
            <View style={{ height: 50, paddingHorizontal: 10, margin: 5, alignItems: "center", justifyContent: "center" }}>
                <Text style={{ fontSize: 22, fontWeight: "600", color: AppColors.header_text_color }}>{leftText}</Text>
            </View>
            <View>
                <Text style={{ fontSize: 22, fontWeight: "600", color: AppColors.header_text_color }}>{centerText}</Text>
            </View>
            <View style={{ height: 50, paddingHorizontal: 10, margin: 5, flexDirection: 'row', alignItems: "center", justifyContent: "center" }}>
                <Text style={{ color: AppColors.header_text_color, fontSize: 16, paddingHorizontal: 5, fontWeight: "600" }}>{rightText}</Text>
                <Image resizeMode="contain" source={downArrow} />
            </View>
        </View >
    );
};

export default HomeScreenHeader;

