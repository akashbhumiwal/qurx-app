import React, { Component } from 'react';
import { StyleSheet, View, Text, Image } from 'react-native';
const filledStarImg = require("../PagesImages/star-filled.png");
const unfilledStarImg = require("../PagesImages/star-unfilled.png");
type Props = {
    ratingObj: {
        ratings: number;
        views: number;
    }
};

export default class StarRating extends Component<Props> {
    render() {
        // Recieve the ratings object from the props
        let ratingObj = this.props.ratingObj;

        // This array will contain our star tags. We will include this
        // array between the view tag.
        let stars = [];
        // Loop 5 times
        for (var i = 1; i <= 5; i++) {
            // Set the path to filled stars
            let path = filledStarImg;
            // If ratings is lower, set the path to unfilled stars
            if (i > ratingObj.ratings) {
                path = unfilledStarImg;
            }
            // Push the Image tag in the stars array
            stars.push((<Image style={styles.image} source={path} />));
        }
        return (
            <View style={styles.container}>
                {stars}
                {/* <Text style={styles.text}>({ratingObj.views})</Text> */}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: { flexDirection: 'row' },
    image: { width: 15, height: 15 }
});