import { Body, Header, Left, Right, Text } from "native-base";
import PropTypes from "prop-types";
import React, { Component } from "react";
import { Image, TouchableOpacity, StatusBar } from "react-native";
import headerStyle from "../styles/AppHeaderStyles";
import AppDimentions from "../themeVariables/AppDimentions";
import { isEmpty, isEmptyField } from "../utility/Utility";
import AppColors from "../themeVariables/AppColors";


// const menuIcon = require("../pages/images/icon-menu.png");
const backArrowIcon = require("../PagesImages/back-arrow-white.png");

const LeftPart = ({ isVisible, showMenu, action, leftIconStyle }) => {
    return (
        <Left style={headerStyle.leftPart}>
            {isVisible ? (
                <TouchableOpacity
                    style={{
                        paddingRight: 20,
                        flex: 1,
                        justifyContent: "center"
                    }}
                    transparent
                    onPressOut={action}
                >
                    {AppDimentions.current_platform && showMenu ? (
                        <Image
                            resizeMode="contain"
                            style={headerStyle.menuButton}
                            source={menuIcon}
                        />
                    ) : (
                            <Image
                                source={showMenu ? menuIcon : backArrowIcon}
                                style={
                                    isEmpty(leftIconStyle)
                                        ? {
                                            height: 20,
                                            width: 25
                                        }
                                        : leftIconStyle
                                }
                            />
                        )}
                </TouchableOpacity>
            ) : null}
        </Left>
    );
};

const Content = ({ title }) => {
    return (
        <Body style={headerStyle.bodyPart}>
            <Text style={headerStyle.textStyle}>{title}</Text>
        </Body>
    );
};
const getDefaultActionTextStyle = () => {
    return headerStyle.rightActionText;
};

const RightPart = ({
    isVisible,
    iconImage,
    rightActionText,
    rightActionTextStyle,
    action,
    isSwitchIconVisible,
    switchDriverCallback,
    switchIcon
}) => {
    return (
        <Right style={headerStyle.rightPart}>
            {isVisible ? (
                <TouchableOpacity onPress={action}>
                    {isEmptyField(rightActionText) ? (
                        <Image
                            resizeMode="contain"
                            style={headerStyle.menuButton}
                            source={iconImage}
                        />
                    ) : (
                            <Text
                                style={
                                    isEmpty(rightActionTextStyle)
                                        ? getDefaultActionTextStyle()
                                        : rightActionTextStyle
                                }
                            >
                                {rightActionText}
                            </Text>
                        )}
                </TouchableOpacity>
            ) : null}
            {isVisible && isSwitchIconVisible ? (
                <SwitchIcon
                    switchIcon={switchIcon}
                    isSwitchIconVisible={isSwitchIconVisible}
                    switchDriverCallback={switchDriverCallback}
                />
            ) : null}
        </Right>
    );
};

export default class NewAppHeader extends Component {
    /**
     * refreshes the components
     */
    callBack() {
        this.props.navigation.goBack();
    }

    openDrawer() {
        this.props.navigation.openDrawer();
    }

    rightActionCallback() {
        this.props.rightActionCallback();
    }

    switchDriverCallback() {
        this.props.switchDriverCallback();
    }

    leftActionCallback() {
        this.props.leftActionCallback();
    }

    render() {
        const { clear = false } = this.props;

        return (
            <Header
                noShadow={true}
                style={[
                    headerStyle.parent,
                    clear && { borderBottomWidth: 0 },
                    this.props.transparent ? { backgroundColor: "transparent" } : {}
                ]}
                androidStatusBarColor={"#16ae7a"}
            >
                <StatusBar backgroundColor={AppColors.blue} barStyle="light-content" />
                <LeftPart
                    isVisible={this.props.isLeftActionVisible}
                    showMenu={this.props.showMenu}
                    leftIconStyle={this.props.leftIconStyle}
                    action={
                        this.props.showMenu
                            ? this.openDrawer.bind(this)
                            : this.props.handleBackAction
                                ? () => this.leftActionCallback()
                                : this.callBack.bind(this)
                    }
                />
                <Content title={this.props.headerText} />
                <RightPart
                    isVisible={this.props.isRightActionVisible}
                    iconImage={this.props.rightIconImage}
                    rightActionText={this.props.rightActionText}
                    rightActionTextStyle={this.props.rightActionTextStyle}
                    action={() => this.rightActionCallback()}
                    switchIcon={this.props.switchIcon}
                    isSwitchIconVisible={this.props.isSwitchIconVisible}
                    switchDriverCallback={() => this.switchDriverCallback()}
                />
            </Header>
        );
    }
}

NewAppHeader.propTypes = {
    isLeftActionVisible: PropTypes.bool.isRequired,
    isRightActionVisible: PropTypes.bool.isRequired,
    navigation: PropTypes.object.isRequired,
    headerText: PropTypes.string.isRequired,
    showMenu: PropTypes.bool,
    rightActionCallback: PropTypes.func,
    rightIconImage: PropTypes.number,
    handleBackAction: PropTypes.bool,
    leftActionCallback: PropTypes.func,
    switchIcon: PropTypes.string,
    transparent: PropTypes.bool
};
