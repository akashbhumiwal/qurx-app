
import React from 'react';
import { SafeAreaView, StyleSheet, Image } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import Homescreen from "../screens/Login/Homescreen";
import MyDoctors from "../screens/Login/MyDoctors"
import Appointments from "../screens/Login/Appointments"
import AccountSettings from "../screens/Login/AccountSettings"
import ProfileSettings from "../screens/Login/ProfileSettings"
import LoginScreen from "../screens/Login/LoginScreen"
// import ForgotPassword from "./screens/Login/ForgotPassword"
import ResetPassword from "../screens/Login/ResetPassword"
import LoginPage from "../screens/Login/LoginPage"
import SignUpScreen from "../screens/Login/SignUpScreen"
import Settings from "../screens/Login/Settings"
import SearchResultsPage from "../screens/Login/SearchResultsScreen"
import FilterScreen from "../screens/Login/FilterScreen"
import OtpScreen from "../screens/Login/OtpScreen"
import { createStackNavigator } from '@react-navigation/stack';
import { UserProvider } from '../utility/UserContext';
const Stack = createStackNavigator();
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import AppColors from '../themeVariables/AppColors'
const Tab = createMaterialBottomTabNavigator();
const searchIcon = require("../PagesImages/icon-home.png");
const focusedSearchIcon = require("../PagesImages/icon-home-active.png");

const myDoctorsIcon = require("../PagesImages/icon-doctors.png");
const focusedmyDoctorsIcon = require("../PagesImages/icon-doctors-active.png");

const appointmentIcon = require("../PagesImages/icon-appointments.png");
const focusedappointmentIcon = require("../PagesImages/icon-appointments-active.png");

const loginIcon = require("../PagesImages/icon-login.png");
const focusedloginIcon = require("../PagesImages/icon-login-active.png");

function Root() {
    return (
        <Stack.Navigator headerMode="none">
            <Stack.Screen name="HomeScreen" component={TabRoutes} />
            <Stack.Screen name="SignUpScreen" component={SignUpScreen} />
            <Stack.Screen name="LoginPage" component={LoginPage} />
            {/* <Stack.Screen name="ForgotPassword" component={ForgotPassword} /> */}
            <Stack.Screen name="ResetPassword" component={ResetPassword} />
            <Stack.Screen name="Settings" component={Settings} />
            <Stack.Screen name="AccountSettings" component={AccountSettings} />
            <Stack.Screen name="ProfileSettings" component={ProfileSettings} />
        </Stack.Navigator>
    );
}


function TabRoutes() {
    return (

        <Tab.Navigator
            shifting={false}
            barStyle={{ backgroundColor: 'white' }}
            activeColor={AppColors.theme_blue_color}
            inactiveColor={AppColors.theme_text_color}
            screenOptions={({ route }) => ({
                tabBarIcon: ({ focused, color, size }) => {
                    let iconName;
                    // if (route.name == "HomeScreen") {
                    if (route.name == "Search") {
                        iconName = focused ? focusedSearchIcon : searchIcon
                        return <Image style={{ height: 25, width: 25, }}
                            source={iconName} />
                    } else if (route.name == "MyDoctors") {
                        iconName = focused ? focusedmyDoctorsIcon : myDoctorsIcon
                        return <Image style={{ height: 25, width: 25, }}
                            source={iconName} />
                    } else if (route.name == "Appointments") {
                        iconName = focused ? focusedappointmentIcon : appointmentIcon
                        return <Image style={{ height: 25, width: 25, }}
                            source={iconName} />
                    } else if (route.name == "Login") {
                        iconName = focused ? focusedloginIcon : loginIcon
                        return <Image style={{ height: 25, width: 25, }}
                            source={iconName} />
                    }
                }
            })}
        >
            {/* <Stack.Screen name="HomeScreen" component={MyStack} /> */}
            <Stack.Screen name="Search" component={MyStack} />
            <Stack.Screen name="MyDoctors" component={MyDoctors} />
            <Stack.Screen name="Appointments" component={Appointments} />
            <Stack.Screen name="Login" component={LoginScreen} />
        </Tab.Navigator>
    );
}

function MyStack() {
    return (
        <Stack.Navigator headerMode="none">
            <Stack.Screen name="HomeScreen" component={Homescreen} />
            <Stack.Screen name="SearchResultsPage" component={SearchResultsPage} />
            <Stack.Screen name="FilterScreen" component={FilterScreen} />
            <Stack.Screen name="OtpScreen" component={OtpScreen} />
            <Stack.Screen name="SignUpScreen" component={SignUpScreen} />
        </Stack.Navigator>
    );
}


const AppNavigator: () => React$Node = () => {
    const user = { name: 'Tania', loggedIn: true }
    return (
        <UserProvider value={user}>
            <NavigationContainer>
                <Root></Root>
            </NavigationContainer>
        </UserProvider>
    );
};

export default AppNavigator;
