import { Platform, Dimensions, PixelRatio } from "react-native";
import {
    responsiveHeight,
    responsiveWidth,
    responsiveFontSize
} from "react-native-responsive-dimensions";
import QurxAppColors from "../themeVariables/QurxAppColors";
// import AppConstants from "./utility/AppConstants";

const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;
const platform = Platform.OS;
const platformStyle = undefined;
const isIphoneX =
    platform === "ios" && deviceHeight === 812 && deviceWidth === 375;
const isIphone_five =
    platform === "ios" && deviceHeight === 568 && deviceWidth === 320;
const isIphone_six =
    platform === "ios" && deviceHeight === 667 && deviceWidth === 375;
const isIphone_Plus = platform === "ios" && deviceHeight === 736;
const scaleDensity = PixelRatio.get();
const isDeviceNote8 =
    platform === "android" && scaleDensity >= 3.5 && deviceHeight >= 735; // Samsung Note 8
const isAndroid = platform !== "ios";
//const note8BoldForgotFontSize = 2.4;
//const note8ForgotSubheadingFontSize = 1.2;

const getFontSize = dimens => {
    if (isDeviceNote8) {
        return dimens * 0.76;
    }
    return dimens;
};

export default {
    current_platform: isAndroid,
    is_device_5: isIphone_five,
    is_device_6: isIphone_six,
    is_device_x: isIphoneX,
    is_device_p: isIphone_Plus,
    title_text: responsiveFontSize(getFontSize(4)),
    thirty: responsiveHeight(4),
    twety_one: 21,
    thirty_seven: 37,
    login_btn_ht: deviceHeight * 0.07,
    text_input_ht: deviceHeight * 0.05,
    input_box_wd: deviceWidth - 80,
    input_box_wd: deviceWidth - 70,
    titlebar_fontsize: 21,
    deviceWidth: deviceWidth,
    deviceHeight: deviceHeight,

    //App Header
    //  titlebar_fontsize: responsiveFontSize(getFontSize(2.3)),
    //  titlebar_fontsize: getHeaderFontSize(),
    // LOGIN_PAGE_DIMENTIONS
    header_text_size: responsiveFontSize(getFontSize(4.5)),
    header_margin: responsiveHeight(5),
    for_drivers_font_size: deviceHeight * 0.023,
    driver_margin: deviceHeight * 0.015,
    body_padding: deviceHeight * 0.03,
    emailbox_top_margin: responsiveHeight(4),
    username_font_size: responsiveFontSize(getFontSize(2)),
    inputbox_height: responsiveHeight(6),
    inputBox_height_passenger_count_quotes: responsiveHeight(14),
    inputbox_padding: responsiveHeight(2),
    inputbox_border_radius: responsiveHeight(0.7),
    username_top_margin: responsiveHeight(6),
    foget_password_font_size: responsiveFontSize(getFontSize(1.7)),
    inputbox_width: responsiveWidth(80),
    send_email_width_charterup: responsiveWidth(70),
    enter_button_top_margin: responsiveHeight(1.5),
    enter_button_height: responsiveHeight(7),
    enter_button_width: responsiveWidth(80),
    button_font_size: responsiveFontSize(getFontSize(2.4)),
    button_text_margin_top: responsiveHeight(0.4),
    reserve_text_margin_top: responsiveHeight(8),
    reserve_text_font_size: responsiveFontSize(getFontSize(2.2)),
    call_button_width: responsiveWidth(65),
    call_button_margin_top: responsiveHeight(1.5),
    call_button_padding: responsiveHeight(3),
    call_icon_height_width: responsiveHeight(2.6),
    password_field_bottom_margin: responsiveHeight(0.4),
    login_input_box_icon_width: isAndroid
        ? scaleDensity <= 2
            ? 20
            : scaleDensity < 3
                ? 25
                : 30
        : 20,
    login_input_box_icon_height: isAndroid
        ? scaleDensity <= 2
            ? 20
            : scaleDensity < 3
                ? 25
                : 30
        : 20,
    login_input_box_icon_margin: 5,
    padding_normal: 5,
    padding_two: 2,
    call_button_text_size: 18,
    logo_height: 30,
    logo_width: scaleDensity < 3 ? 140 : 150,

    charterup_input_btn_radius: responsiveHeight(4),
    charterup_enter_button_width: responsiveWidth(70),
    charterup_forgetp_margin_left: responsiveWidth(14),

    map_tab_radius: 14,


    getNavigationButtonHeight() {
        if (this.current_platform) {
            return responsiveHeight(9.2);
        } else {
            if (this.is_device_x) {
                return responsiveHeight(7);
            } else {
                return responsiveHeight(8.4);
            }
        }
    },

    getUpcommingListMargin() {
        if (this.current_platform) {
            return responsiveHeight(0.3);
        } else {
            return responsiveHeight(2);
        }
    },

    getUpcommingCellHeight() {
        if (this.current_platform) {
            return responsiveHeight(17);
        } else {
            return responsiveHeight(14.5);
        }
    },

    getActiveForwardArrowStyle() {
        if (this.current_platform) {
            return (style = {
                backgroundColor: QurxAppColors.orange,
                // backgroundColor: LogicalVariable.colors.activeUpcomingTrip,
                width: responsiveWidth(12),
                justifyContent: "center",
                alignItems: "center",
                borderTopRightRadius: responsiveWidth(2),
                borderBottomRightRadius: responsiveWidth(2)
            });
        } else {
            return (style = {
                // backgroundColor: LogicalVariable.colors.activeUpcomingTrip,
                backgroundColor: QurxAppColors.orange,
                width: responsiveWidth(12),
                justifyContent: "center",
                alignItems: "center",
                borderTopRightRadius: responsiveWidth(2),
                borderBottomRightRadius: responsiveWidth(2)
            });
        }
    },

    getInActiveForwardArrowStyle() {
        if (this.current_platform) {
            return (style = {
                // backgroundColor: LogicalVariable.colors.inActiveUpcomingTrip,
                backgroundColor: QurxAppColors.orange,
                width: responsiveWidth(12),
                justifyContent: "center",
                alignItems: "center",
                borderTopRightRadius: responsiveWidth(2),
                borderBottomRightRadius: responsiveWidth(2)
            });
        } else {
            if (this.is_device_x) {
                return (style = {
                    //   backgroundColor: LogicalVariable.colors.inActiveUpcomingTrip,
                    backgroundColor: QurxAppColors.orange,
                    width: responsiveWidth(12),
                    justifyContent: "center",
                    alignItems: "center",
                    borderTopRightRadius: responsiveWidth(2),
                    borderBottomRightRadius: responsiveWidth(2)
                });
            } else {
                return (style = {
                    //   backgroundColor: LogicalVariable.colors.inActiveUpcomingTrip,
                    backgroundColor: QurxAppColors.orange,
                    width: responsiveWidth(12),
                    justifyContent: "center",
                    alignItems: "center",
                    borderTopRightRadius: responsiveWidth(2),
                    borderBottomRightRadius: responsiveWidth(2)
                });
            }
        }
    },

    getReadMoreFontSize() {
        responsiveFontSize(getFontSize(2));
    },

    getDetailCardStyle() {
        if (this.current_platform) {
            return (style = {
                paddingLeft: 15,
                paddingTop: responsiveHeight(1),
                paddingBottom: responsiveHeight(1),
                marginLeft: responsiveWidth(4),
                marginRight: responsiveWidth(4),
                paddingLeft: 20,
                paddingRight: 20,
                marginTop: responsiveHeight(2.5),
                backgroundColor: "white"
            });
        } else {
            return (style = {
                padding: 15,
                marginLeft: responsiveWidth(4),
                marginRight: responsiveWidth(4),
                paddingLeft: 20,
                paddingRight: 20,
                marginTop: responsiveHeight(2.5),
                backgroundColor: "white"
            });
        }
    },

    getCardDetialUpcomingListIphoneX() {
        return (style = {
            paddingLeft: 10,
            marginLeft: this.is_device_x ? 15 : 0,
            marginRight: this.is_device_x ? 15 : 0,
            marginTop: isAndroid ? 15 : 15,
            backgroundColor: "white"
        });
    },

    getGrayCard() {
        return (style = {
            paddingLeft: 10,
            marginLeft: this.is_device_x ? 15 : 0,
            marginRight: this.is_device_x ? 15 : 0,
            marginTop: isAndroid ? 5 : 15,
            backgroundColor: "#d0d7e2"
        });
    },

    // used in Upcoming Trips and Active Trip
    getCardDetialScreen() {
        return (style = {
            paddingLeft: 10,
            marginLeft: 15,
            marginRight: 15,
            marginTop: isAndroid ? 5 : 15,
            paddingRight: 10,
            backgroundColor: "white"
        });
    },

    getTripDetailCardStyle() {
        if (this.current_platform) {
            return (style = {
                backgroundColor: "white",
                marginTop: responsiveHeight(1),
                marginLeft: responsiveWidth(4),
                marginRight: responsiveWidth(4)
            });
        } else {
            return (style = {
                backgroundColor: "white",
                marginTop: responsiveHeight(2.5),
                marginLeft: responsiveWidth(4),
                marginRight: responsiveWidth(4)
            });
        }
    },

    // used in Tripdetails
    getCardStopStyle() {
        if (this.current_platform) {
            return (style = {
                padding: 10,
                backgroundColor: "white",
                marginLeft: responsiveWidth(4),
                marginRight: responsiveWidth(4),
                marginBottom: responsiveHeight(10),
                marginTop: responsiveHeight(1)
            });
        } else {
            if (this.is_device_x) {
                return (style = {
                    padding: 10,
                    backgroundColor: "white",
                    marginLeft: responsiveWidth(4),
                    marginRight: responsiveWidth(4),
                    marginBottom: responsiveHeight(9),
                    marginTop: responsiveHeight(2)
                });
            } else {
                return (style = {
                    padding: 10,
                    backgroundColor: "white",
                    marginLeft: responsiveWidth(4),
                    marginRight: responsiveWidth(4),
                    marginBottom: responsiveHeight(9),
                    marginTop: responsiveHeight(2.5)
                });
            }
        }
    },



    getButtonStyle() {
        if (this.current_platform) {
            return {
                color: "white",
                fontSize: responsiveFontSize(getFontSize(2.4)),
                padding: 0,
                // fontFamily: "lato-bold",
                alignSelf: "center",
                marginTop: responsiveHeight(0.4),
                marginBottom: responsiveHeight(1)
            };
        } else {
            if (this.is_device_x) {
                return {
                    color: "white",
                    fontSize: 20,
                    padding: 0,
                    // fontFamily: "lato-bold",
                    alignSelf: "center"
                };
            } else if (this.is_device_p) {
                return {
                    color: "white",
                    fontSize: 20,
                    padding: 0,
                    // fontFamily: "lato-bold",
                    alignSelf: "center"
                };
            } else {
                return {
                    color: "white",
                    fontSize: this.is_device_5 ? 16 : 18,
                    padding: 0,
                    // fontFamily: "lato-bold",
                    alignSelf: "center"
                };
            }
        }
    },

    getEnterButtonStyle() {
        if (this.current_platform) {
            return {
                color: "white",
                fontSize: responsiveFontSize(getFontSize(2.4)),
                padding: 0,
                // fontFamily: "lato-bold",
                alignSelf: "center",
                marginTop: responsiveHeight(0.4),
                marginBottom: responsiveHeight(1)
            };
        } else {
            if (this.is_device_x) {
                return {
                    color: "white",
                    fontSize: 20,
                    paddingTop: 8,
                    // fontFamily: "lato-bold",
                    alignSelf: "center"
                };
            } else if (this.is_device_p) {
                return {
                    color: "white",
                    fontSize: 20,
                    paddingTop: 8,
                    // fontFamily: "lato-bold",
                    alignSelf: "center"
                };
            } else {
                return {
                    color: "white",
                    fontSize: this.is_device_5 ? 16 : 18,
                    paddingTop: this.is_device_5 ? 2 : 5,
                    // fontFamily: "lato-bold",
                    alignSelf: "center"
                };
            }
        }
    },

    getHeaderFontSize() {
        if (this.current_platform) {
            return responsiveFontSize(getFontSize(2.4));
        } else {
            if (this.is_device_x) {
                return 25;
            } else if (this.is_device_p) {
                return 25;
            } else {
                return this.is_device_5 ? 19 : 21;
            }
        }
    },

    getActionTextFontSize() {
        if (this.current_platform) {
            return responsiveFontSize(getFontSize(2.4));
        } else {
            if (this.is_device_x) {
                return 16;
            } else if (this.is_device_p) {
                return 16;
            } else {
                return this.is_device_5 ? 10 : 12;
            }
        }
    },

    getSignaurePanStyle() {
        if (this.current_platform) {
            return (style = {
                flexDirection: "row",
                justifyContent: "space-around",
                alignContent: "center",
                marginLeft: 18,
                marginRight: 18,
                marginTop: 60,
                bottom: deviceHeight <= 592 ? responsiveHeight(11) : responsiveHeight(8)
            });
        } else {
            return (style = {
                flexDirection: "row",
                justifyContent: "space-around",
                alignContent: "center",
                marginLeft: 18,
                marginRight: 18,
                marginTop: responsiveHeight(6),
                bottom: responsiveHeight(2)
            });
        }
    },

    getDestinationMarkerHeight() {
        if (this.current_platform) {
            return responsiveHeight(2);
        } else {
            return responsiveHeight(1);
        }
    },

    getDestinationMarkerStyle() {
        if (this.current_platform) {
            return (style = {
                height: responsiveHeight(1),
                width: responsiveHeight(1)
            });
        } else {
            return (style = {
                height: responsiveHeight(10),
                width: responsiveHeight(10)
            });
        }
    },

    getOverLayColor() {
        return "rgba(96,102,115, .7)";
    }
};
