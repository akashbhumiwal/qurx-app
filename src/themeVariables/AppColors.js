//Qurx colors codes
// Color variables here.



var blueColor = "#259cf7";
var blueColor_disabled = "#bae0fd";
var charcoalColor = "#444444";
export default {
  ////HOMESCREEN COLORS///
  screenBgColor: "#f6f8fa",
  header_text_color: "#1c93c8",
  theme_text_light_grey_color: "#bac2c6",
  theme_text_color: "#75868e",
  theme_blue_color: "#1c93c8",



  // BS Color Vars
  gray_base: "#000",
  gray_darker: "lighten($gray-base, 13.5%)", // #222
  gray_dark: "#3e4351",
  gray: "lighten($gray-base, 33.5%)", // #555
  gray_light: "#6c717c", // #777
  gray_medium_light: "#bababa",
  gray_mid_light: "#CACDD3",
  gray_lighter: "#e8e8e8", // #eee

  red: "#e10055",
  orange: "#ea7721",
  yellow: "#f5b700",
  yellow_pale: "lighten($yellow,35)",
  green: "#7cd074",
  teal: "#27d3c4",
  blue_black: "#044e8f",
  blue_dark: "#218cdd",
  blue: "#259cf7",
  blueNew: "#16ae7a",
  blue_light: "#edf4fa",
  blue_pale: "#f4f9fd",
  blue_dull: "#accbe3",
  purple: "#6a63e8",
  magenta: "#b25ae0",
  not_availabe: "#e3e4e5", //'#87CEFA',

  // Customer color vars
  black: "#3e4351",
  white: "#fff",

  AppBg: "#e7eff6",
  listingBg: "#e7eff6",

  pickupinfo_text_color: "#3E4351",
  hint_color: "#666",
  MapBg: "#00e7eff6",
  transparent: "00000000",
  charcoal: charcoalColor,
  black_original: "#000000",
  gainsBoro: "#ddd",

  // login
  loginButton: blueColor,
  loginInputBoxBG: "#fff", // same as white
  loginInputBoxBorder: "#ddd", // same as gainsBoro
  appNameFirstPart: charcoalColor,
  appNameSecondPart: blueColor,
  headerColor: blueColor,
  callButtonBorder: blueColor,
  // upcoming trips
  activeUpcomingTrip: blueColor,
  inActiveUpcomingTrip: "#e3e4e5",
  //header
  statusBar: blueColor,
  // trip details
  tripDetailsBottomButton: "#FF6600",
  tripDetailsBottomButtonWithAplha: "#CCFF6600",
  loginButton_disabled: blueColor_disabled,
  pinColor: "rgb(0, 130, 230)",
  swipeCardColor: "rgb(25, 123, 208)",
  cardHeaderFontColor: "rgb(216, 215, 216)",
  cardInput: "argb(229,238,247)",
  // fontLatoBold: "lato-bold",
  fontRegular: "sfuitext-regular",
  enabledOptionColor: "rgb(230,237,244)",
  disabledOptionColor: "rgb(47,128,195)",
  // bid details
  bidProgressColor: "#dc0042"
};
