
//CharterApp colors codes
// Color variables here.
var orangeColor = '#F35326';
var orangeColor_disabled = '#f57956';
var blueColor = '#08A6F0'
export default {
    // BS Color Vars
    gray_base: '#000',
    gray_darker: 'lighten($gray-base, 13.5%)', // #222
    gray_dark: '#3e4351',
    gray: 'lighten($gray-base, 33.5%)', // #555
    gray_light: '#6c717c', // #777
    gray_medium_light: '#bababa',
    gray_mid_light: '#CACDD3',
    gray_lighter: '#e8e8e8', // #eee

    red: '#e10055',
    orange: '#ea7721',
    yellow: '#f5b700',
    yellow_pale: 'lighten($yellow,35)',
    green: '#7cd074',
    teal: '#27d3c4',
    blue_black: '#044e8f',
    blue_dark: '#218cdd',
    blue: '#08A6f0',
    blue_light: '#edf4fa',
    blue_pale: '#f4f9fd',
    blue_dull: '#accbe3',
    purple: '#6a63e8',
    magenta: '#b25ae0',
    not_availabe: '#e3e4e5',//'#87CEFA',

    // Semantic BS Colors
    // brand-primary:         $blue;
    // brand-success:         $green;
    // brand-info:            $blue-light;
    // brand-warning:         $orange;
    // brand-danger:          $red;

    // Customer color vars
    black: blueColor,
    white: '#fff',

    //AppBg: '#F4F8FC',
    AppBg: '#FFFFFF',
    listingBg: '#e7eff6',
    lightBlue: '#F4F8FC',


    pickupinfo_text_color: '#3E4351',
    hint_color: '#666',
    MapBg: '#00e7eff6',
    transparent: '00000000',
    charcoal: '#444444',
    black_original: '#000000',
    gainsBoro: '#ddd',

    // login
    loginButton: orangeColor,
    loginInputBoxBG: '#F4F8FC',  // same as AppBg
    loginInputBoxBorder: '#A1B8CC',
    appNameFirstPart: blueColor,
    appNameSecondPart: '#3C3C3C',
    headerColor: '#16ae7a',
    callButtonBorder: orangeColor,
    // upcoming trips
    activeUpcomingTrip: orangeColor,
    inActiveUpcomingTrip: '#A1B8CC',
    statusBar: '#2D2D2D',
    // trip details
    tripDetailsBottomButton: orangeColor,
    tripDetailsBottomButtonWithAplha: '#F2F35326',
    loginButton_disabled: orangeColor_disabled,
    pinColor: 'rgb(0, 148, 220)'
}
