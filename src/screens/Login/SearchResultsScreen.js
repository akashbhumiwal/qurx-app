import { Text } from "native-base";
import React, { Component } from "react";
import { Alert, Image, View, TouchableOpacity, Dimensions, Linking, Platform, ActivityIndicator } from "react-native";
import AppColors from "../../themeVariables/AppColors"
import AxiosApiHandler from "../../networkHandler/AxiosApiHandler";
import NewAppHeader from "../../components/NewAppHeader";
import SearchResultsStyle from "../../styles/SearchResultsStyle";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import { TextInput } from "react-native-paper";
import Loader from '../../utility/Loader';
import StarRating from '../../components/StarRating';
import { FlatList, ScrollView } from "react-native-gesture-handler";
import StringConstants from "../../utility/StringConstants";
const { width, height } = Dimensions.get("window");
const searchIcon = require("./../../PagesImages/search-icon.png");
const docDemoImg = require("../../PagesImages/doc-image.png");
const verifiedImg = require("../../PagesImages/icon-verified.png");
const callImg = require("../../PagesImages/call-button.png");
const filterImg = require("../../PagesImages/icon-filter.png");
// Use iPhone6 as base size which is 375 x 667
const baseWidth = 375;
const baseHeight = 667;
var hit = false;
const scaleWidth = width / baseWidth;
const scaleHeight = height / baseHeight;
const scale = Math.min(scaleWidth, scaleHeight);
const ratingObj = { ratings: 0, views: 34000 }
// var ratingObj = 0;AppColors.blue_dark
var isMapSelected = false;
var mapSelectedColor = AppColors.theme_text_color;
export const scaledSize = size => Math.ceil(size * scale);
export class SearchResultsScreen extends Component<{}> {

    constructor(props) {
        super(props);
        this.onEndReachedCalledDuringMomentum = true;
        // Text.defaultProps.allowFontScaling = false;
        this.state = {
            username_input: false,
            searchText: "",
            responseArray: [],
            loading: false,
            fetching_from_server: false,
            onEndReachedCalledDuringMomentum: true,
            fetchingStatus: false,
            isLoading: false,
            page: 0,
        };
    }


    dialCall = () => {
        let phoneNumber = '';
        if (Platform.OS === 'android') {
            phoneNumber = 'tel:${1234567890}';
        }
        else {
            phoneNumber = 'telprompt:${1234567890}';
        }
        Linking.openURL(phoneNumber);
    };

    bookAppointmentClicked = () => {
        Alert.alert("Work in progress")
    }

    componentDidMount() {
        var searchedText = this.props.route.params.searchText;
        this.setState({ searchText: this.props.route.params.searchText });
        this.getSearchDoctorDetails();
    }

    createReqObject() {

        let reqObj = {
            "dataActionType": "RETRIVE",
            "qurxRoleContext": 2,
            "searchRequest": {
                "from": this.state.page,
                "size": 5,
                "sort": {
                    "consultationFee": {
                        "order": "desc"
                    }
                },
                "query": {
                    "multi_match": {
                        // "query": "Dr. Pankaj Sharma",
                        "query": "clinic",
                        "fields": ["firstName", "lastName", "medicalBio", "addressLine1",
                            "addressLine2", "city", "zipCode", "state", "careSiteType",
                            "careSiteName", "phoneNumber", "medicalDegree", "specialization"]
                    }
                }
            }
        }
        return reqObj

    }

    getSearchDoctorDetails() {
        if (this.state.isLoading) {
            return;
        }
        this.setState({ loading: true });
        var newreqBody = this.createReqObject();
        var pagenum = this.state.page;
        console.log("the page num is" + pagenum);
        AxiosApiHandler.getInstance().searchDoctors(newreqBody, this.state.page)
            .then((response) => {
                this.setState({ loading: false })
                this.setState({ isLoading: false })
                if (response.response.data.data.length > 0) {
                    // this.setState({ responseArray: response.response.data.data });
                    this.setState({ responseArray: [...this.state.responseArray, ...response.response.data.data], isLoading: false });
                } else {
                    this.setState({ loading: false })
                    this.setState({ isLoading: false })
                    setTimeout(() => Alert.alert(StringConstants.QURX_APP_ALERT, "No more results"), 2000)
                    // this.showAlert();
                }
            }).catch((error) => {
                this.setState({ loading: false })
                this.setState({ isLoading: false })
                var message = error.error.response.data.data[0]
                if (message) {
                    this.setState({ loading: false })
                    setTimeout(() => Alert.alert(message), 2000)
                } else {
                    this.setState({ loading: false })
                    setTimeout(() => Alert.alert("Bad Request"), 2000)
                }

            })
    }
    customSearchBar() {
        return (
            <View style={{ flexDirection: "row", borderColor: AppColors.gray_medium_light, borderWidth: 0.9, marginHorizontal: 20, margin: 10 }}>
                <Image style={SearchResultsStyle.searchImgStyles} resizeMode="contain" source={searchIcon} />
                <TextInput style={{ borderBottomWidth: 0, height: 35, flex: 1, backgroundColor: "white" }}
                    placeholder={this.state.searchText}
                    onChangeText={text => this.searchItems(text)}
                    value={this.state.value} />
            </View>
        )
    }


    navToFilterPage() {
        this.props.navigation.navigate('FilterScreen');
    }


    filterView() {
        return (
            <View style={SearchResultsStyle.filterParentViewStyles}>
                <TouchableOpacity
                    onPress={() => { this.navToFilterPage(); }}
                    style={SearchResultsStyle.filterBtnStyles}>
                    <Image style={SearchResultsStyle.searchImgStyles} resizeMode="contain" source={filterImg} />
                    <Text style={SearchResultsStyle.filterTextStyles}>Filter</Text>
                </TouchableOpacity>
                <TouchableOpacity
                    onPress={() => { this.mapClicked() }}>
                    <Text style={{ color: mapSelectedColor, fontSize: 18, margin: 5, fontWeight: "600" }}>Map</Text>
                </TouchableOpacity>
            </View>
        )
    }

    renderItem(item) {
        // console.log("the item is" + item);
        ratingObj.ratings = Math.round(item.item._score);
        return (
            <View style={SearchResultsStyle.cellParentStyles}>
                <TouchableOpacity activeOpacity={0.8}
                    style={SearchResultsStyle.parentCardView}>
                    <View style={SearchResultsStyle.childCardView}>
                        <View style={{ width: "22%", }}>
                            <Image source={docDemoImg} style={SearchResultsStyle.roundImgStyles}></Image>
                        </View>
                        <View style={{ width: "80%", flexDirection: "column" }}>
                            <View style={{ width: "80%", flexDirection: "row" }}>
                                <Text style={SearchResultsStyle.nameTextStyles}>{"Dr. " + item.item._source.firstName + " " + item.item._source.lastName}</Text>
                                <Image source={verifiedImg} style={SearchResultsStyle.verifiedImgStyles}></Image>
                            </View>
                            <Text style={SearchResultsStyle.professionTextStyles}>{item.item._source.medicalDegree + " | " + item.item._source.primarySpeciality}</Text>
                            <View style={SearchResultsStyle.container}>
                                <StarRating ratingObj={ratingObj} />
                            </View>
                            <Text style={SearchResultsStyle.feeTextStyles}>{"Consultation Fee:" + item.item._source.consultationFee}</Text>
                            {/* <Text style={SearchResultsStyle.adressTextStyles}>{item.item._source.clinicAddress.addressLine1 + item.item._source.clinicAddress.addressLine2 + "," + item.item._source.clinicAddress.state}</Text> */}
                            <Text style={SearchResultsStyle.adressTextStyles}>{"A-5 Evon Technologies IT Park, Sahastradhara road, Dehradun, Uttarakhand"}</Text>
                            <View style={{ flexDirection: "row" }}>
                                <TouchableOpacity style={SearchResultsStyle.submit, { backgroundColor: AppColors.theme_blue_color }}>
                                    <Text style={SearchResultsStyle.buttonText}>Book Appointment Time</Text>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={this.dialCall} style={SearchResultsStyle.submitCall}>
                                    <Image source={callImg} style={SearchResultsStyle.callImgStyles}></Image>
                                    <Text style={SearchResultsStyle.buttonText}>Contact Clinic</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </TouchableOpacity>
            </View>
        )
    }

    backClicked = () => {
        this.props.navigation.pop();
    }

    mapClicked = () => {
        if (isMapSelected === true) {
            isMapSelected = false;
            mapSelectedColor = AppColors.theme_text_color
            this.forceUpdate();
        } else if (isMapSelected === false) {
            isMapSelected = true;
            mapSelectedColor = AppColors.blue_dark
            this.forceUpdate();
        }
    }

    renderMapView = () => {
        return (
            <View style={SearchResultsStyle.parentMapView}>
                <NewAppHeader headerText="Search Results"
                    isLeftActionVisible={true}
                    leftActionCallback={() => {
                        this.backClicked();
                    }}
                    navigation={this.props.navigation} />
                {this.customSearchBar()}
                {this.filterView()}
                <Loader loading={this.state.loading} />
                <KeyboardAwareScrollView
                    scrollEnabled={true}
                    showsVerticalScrollIndicator={false}
                    enableOnAndroid={true}
                    enableAutomaticScroll={true}
                    resetScrollToCoords={{ x: 0, y: 0 }}
                    innerRef={ref => { this.scroll = ref; }}
                    keyboardShouldPersistTaps="handled">
                    <View style={{ width: "90%", marginTop: 170, alignSelf: "center", alignItems: "center", padding: 10, borderColor: AppColors.gray_mid_light, borderWidth: 5 }}>
                        <Text style={{ fontSize: 30, color: AppColors.theme_text_color }}>MAPS</Text>
                        <Text style={{ fontSize: 30, color: AppColors.theme_text_color }}>WORK IN PROGRESS</Text>
                    </View>
                </KeyboardAwareScrollView>
            </View >
        )
    }

    //////////////////////////////////PAGINATION SECTION///////////////////////////////////////////////////////////
    handleLoadMore = () => {
        // if (this.state.isLoading) {
        //     return;
        // }
        console.log("the page number" + this.state.page);
        this.setState({ page: this.state.page + 4 },
            () => { this.getSearchDoctorDetails() })
    }


    renderFooter = () => {
        return (
            <View>
                <ActivityIndicator animating size="large" />
            </View>
        )
    }
    ///////////////////////////////////////////////////////////
    renderDefaultView() {
        return (
            <View style={SearchResultsStyle.parentView}>
                <NewAppHeader headerText="Search Results"
                    isLeftActionVisible={true}
                    leftActionCallback={() => {
                        this.backClicked();
                    }}
                    navigation={this.props.navigation} />
                <View style={{ borderColor: AppColors.gray_lighter, borderBottomWidth: 1.5, }}>
                    {this.customSearchBar()}
                    {this.filterView()}
                </View>
                <Loader loading={this.state.loading} />
                <FlatList
                    style={{ flex: 1, backgroundColor: AppColors.gray_lighter, marginBottom: scaledSize(28) }}
                    itemDimension={180}
                    extraData={this.state.responseArray}
                    data={this.state.responseArray}
                    onEndReached={this.handleLoadMore.bind(this)}
                    renderItem={item => (this.renderItem(item))} />
            </View>
        )
    }

    // renderDefaultView = (state) => {
    //     return (
    //         <View style={SearchResultsStyle.parentView}>
    //             <NewAppHeader headerText="Search Results"
    //                 isLeftActionVisible={true}
    //                 leftActionCallback={() => {
    //                     this.backClicked();
    //                 }}
    //                 navigation={this.props.navigation} />
    //             <View style={{ borderColor: AppColors.gray_lighter, borderBottomWidth: 1.5, }}>
    //                 {this.customSearchBar()}
    //                 {this.filterView()}
    //             </View>

    //             <Loader loading={this.state.loading} />
    //             <View>
    //                 {/* FlatList View */}
    //                 {/* <ScrollView style={{ paddingBottom: 55 }}> */}
    //                 <FlatList
    //                     style={{ flex: 1, backgroundColor: AppColors.gray_lighter, marginBottom: scaledSize(28) }}
    //                     itemDimension={180}
    //                     extraData={this.state.responseArray}
    //                     data={this.state.responseArray}
    //                     // ListFooterComponent={this.renderFooter}
    //                     // onEndReachedThreshold={0.5}
    //                     onEndReached={this.handleLoadMore.bind(this)}
    //                     onMomentumScrollBegin={() => { this.onEndReachedCalledDuringMomentum = false; }}
    //                     // onEndReachedThreshold={() => this.handleLoadMore()}
    //                     renderItem={item => (this.renderItem(item))} />
    //                 {/* </ScrollView> */}
    //             </View>
    //         </View >
    //     );

    // }

    render() {
        if (this.state.isLoading) {
            <View style={{ flex: 1, padding: 20, alignItems: "center", justifyContent: "center" }}>
                <ActivityIndicator animating size="large" />
            </View>
        }
        if (isMapSelected == true) {
            return (
                this.renderMapView()
            )
        } else {
            return (
                this.renderDefaultView(this.state)
            )
        }
    }
}



export default SearchResultsScreen;


    // var reqBody = { "dataActionType": "RETRIVE", "qurxRoleContext": 2, "term": searchedText }
        // var reqBody = { "dataActionType": "RETRIVE", "qurxRoleContext": 2, "term": "Dr. Pankaj Sharma" }