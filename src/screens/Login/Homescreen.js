import { Text } from "native-base";
import React, { Component } from "react";
import { Image, View, StatusBar, TouchableOpacity, TextInput, ScrollView, FlatList } from "react-native";
import AppColors from "../../themeVariables/AppColors"
import HomeScreenHeader from "../../components/HomeScreenHeader"
import HomeScreenStyles from "../../styles/HomeScreenStyles"
import AxiosApiHandler from "../../networkHandler/AxiosApiHandler";
import Loader from '../../utility/Loader';
import Utility from "../../utility/Utility";
import StringConstants from "../../utility/StringConstants";
import AsyncStorage from "@react-native-community/async-storage";
const searchIcon = require("./../../PagesImages/search-icon.png");
var imageArray = [
    require("../../PagesImages/icon-ent.png"),
    require("../../PagesImages/icon-obgyn.png"),
    require("../../PagesImages/icon-doctors-active.png"),
    require("../../PagesImages/icon-dermatologist.png"),
    require("../../PagesImages/icon-psychiatrist.png"),
    require("../../PagesImages/icon-primaryCare.png"),
    require("../../PagesImages/search-icon.png"),
    require("../../PagesImages/icon-eye.png"),
    require("../../PagesImages/icon-ent.png"),
    require("../../PagesImages/icon-obgyn.png"),
]

export class Homescreen extends Component<{}> {

    constructor(props) {
        super(props);
        Text.defaultProps.allowFontScaling = false;
        this.state = {
            loading: false,
            responseArray: [],
            searchText: "",
        };
    }

    componentDidMount() {
        StatusBar.setHidden(true);
        removeValue = async () => {
            try {
                await AsyncStorage.removeItem(StringConstants.QREX_ROLE_CONTEXT);
            } catch (e) {
                // remove error
            }
            // console.log('Done.')
        }
        this.getSettingDetails();
    }

    getSettingDetails() {
        this.setState({ loading: true });
        var reqBody = { "dataActionType": "RETRIVE", "content": { "allowed_value_type": "allowed_roles" } }
        // var reqBody = { "dataActionType": "RETRIVE", "content": { "allowed_value_type": "specialization" } }
        AxiosApiHandler.getInstance().getSpecialityList(reqBody)
            .then((response) => {
                this.setState({ loading: false })
                for (let i = 0; i < response.response.data.data.length; i++) {
                    response.response.data.data[i].imgUrl = imageArray[i];
                }
                this.setState({ responseArray: response.response.data.data });
                this.state.responseArray.forEach(element => {
                    if (element.allowed_role_value == "patient") {
                        Utility.getInstance().storeQrexData(StringConstants.QREX_ROLE_CONTEXT, JSON.stringify(element.id));
                    }
                });
                this.forceUpdate();
            }).catch((error) => {
                console.log("error izzz" + error)
            })
    }

    openSearchPage() {
        this.props.navigation.navigate("SearchResultsPage", { searchText: this.state.searchText, isNewSearch: true });
    }

    renderItem(item) {
        return (
            <View style={{ padding: 8 }}>
                <TouchableOpacity activeOpacity={0.8}
                    style={HomeScreenStyles.parentCardView}>
                    <View style={HomeScreenStyles.childCardView}>
                        <Image style={HomeScreenStyles.imgStyles} source={item.item.imgUrl} />
                        <Text style={{ color: AppColors.theme_text_color, fontSize: 18, fontWeight: "600" }}>{item.item.allowed_role_value.charAt(0).toUpperCase() + item.item.allowed_role_value.slice(1)}</Text>
                    </View>
                </TouchableOpacity>
            </View>
        )
    }


    //     render() {
    //         return (
    //             <View style={HomeScreenStyles.parentView}>
    //                 <HomeScreenHeader leftText={"Search Doctors"} rightText={"Dehradun"}></HomeScreenHeader>
    //                 <Loader loading={this.state.loading} />
    //                 <View style={{ marginTop: 50, alignSelf: "center" }}>
    //                     <Text style={{ marginTop: 40, fontSize: 30, color: AppColors.theme_text_color }}>WORK IN PROGRESS</Text>
    //                 </View>
    //             </View>
    //         );
    //     }
    // }

    render() {
        return (
            <View style={HomeScreenStyles.parentView}>
                <HomeScreenHeader leftText={"Search Doctors"} rightText={"Dehradun"}></HomeScreenHeader>
                <Loader loading={this.state.loading} />
                {/* {CustomSearchBar} */}
                <View style={HomeScreenStyles.searchParentViewStyles}>
                    <TouchableOpacity style={HomeScreenStyles.searchBtnStyles}
                        onPress={() => this.openSearchPage()}>
                        <Image style={HomeScreenStyles.searchImgStyles} resizeMode="contain" source={searchIcon} />
                        <TextInput
                            autoCorrect={false}
                            underlineColorAndroid='transparent'
                            style={HomeScreenStyles.textInputStyles}
                            onChangeText={text => { this.setState({ searchText: text }) }}
                            // value={searchText}
                            placeholder={"  Search"}>
                        </TextInput>
                    </TouchableOpacity>
                </View>
                <Text style={HomeScreenStyles.headingStyles}>Search by specialities</Text>
                <ScrollView style={{ marginBottom: 80 }}>
                    <View style={{ justifyContent: "center", alignItems: "center" }}>
                        <FlatList
                            horizontal={false}
                            numColumns={2}
                            data={this.state.responseArray}
                            renderItem={item => (this.renderItem(item))} />
                    </View>
                </ScrollView>
            </View>
        );
    }
}

export default Homescreen;

