import { Button, Container, Content, Text } from "native-base";
import React, { Component } from "react";
import { Alert, Animated, Image, Keyboard, View, StatusBar, StyleSheet, TouchableOpacity, TouchableHighlight, Dimensions, TextInput, ScrollView } from "react-native";
import AxiosApiHandler from "../../networkHandler/AxiosApiHandler";
import NewAppHeader from "../../components/NewAppHeader";
const arrowBtn = require("../../PagesImages/arrow-button.png");
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import { SlideAnimation } from 'react-native-popup-dialog';
import ResetPasswordStyles from "../../styles/ResetPasswordStyles";
import Loader from "../../utility/Loader";
import StringConstants from "../../utility/StringConstants";
var QruxContext = 0;
const { width, height } = Dimensions.get("window");
// Use iPhone6 as base size which is 375 x 667
const baseWidth = 375;
const baseHeight = 667;
new SlideAnimation({
    initialValue: 0, // optional
    slideFrom: 'bottom', // optional
    useNativeDriver: true, // optional
})
const scaleWidth = width / baseWidth;
const scaleHeight = height / baseHeight;
const scale = Math.min(scaleWidth, scaleHeight);
var userName = "";
var selectedType = "";
var message = "";
var messageOtp = "We have sent an OTP to your mobile number, please verify that OTP and generate new password."
var messageEmail = "We have sent an email to your mobile number, please verify that OTP and generate new password."
export const scaledSize = size => Math.ceil(size * scale);
export class ResetPassword extends Component<{}> {

    constructor(props) {
        super(props);
        Text.defaultProps.allowFontScaling = false;
        this.state = {
            loading: false,
            enteredOtp: "",
            password: "",
            confirmPassword: "",
            // isDialogVisible: false,
        };
    }

    componentDidMount() {
        userName = this.props.route.params.usernameTosend;
        selectedType = this.props.route.params.selectedOption;
        if (selectedType == "email") {
            message = messageEmail;
        } else {
            message = messageOtp;
        }
        console.log("the details are", userName);
        this.forceUpdate();
    }
    backClicked = () => { this.props.navigation.pop(); }

    validateResetPassword() {
        var message = '';
        let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if (this.state.enteredOtp.replace(/\s/g, '').length == 0) {
            message = StringConstants.ENTER_OTP_MESSAGE;
        } else if (this.state.password.replace(/\s/g, '').length == 0 || (this.state.password.length < 8)) {
            // message = 'Please enter a valid password a minimum of 8 digits'
            message = StringConstants.ENTER_VALID_DIGITS_PASSWORD_MESSAGE;
        }
        else if (this.state.confirmPassword.replace(/\s/g, '').length == 0) {
            // message = 'Please re enter the password '
            message = StringConstants.REENTER_PASSWORD_MESSAGE;
        }
        else if (this.state.password !== this.state.confirmPassword) {
            // message = 'Passwords does not match !'
            message = StringConstants.PASSWORD_MISMATCH_MESSAGE;
        }
        if (message == '') {
            return true;
        }
        Alert.alert(StringConstants.QURX_APP_ALERT, message);
        return false;

    }
    confirmClicked() {
        if (this.validateResetPassword() == true) {
            // Alert.alert("Validation success")
            this.updatePasswordRequest();
        }
    }


    updatePasswordRequest() {
        QruxContext = 2;
        this.setState({ loading: true })
        var reqBody = {
            "dataActionType": "UPDATE",
            "qurxRoleContext": QruxContext,
            "userName": userName,
            "content": { "password": this.state.password, "OTP": this.state.enteredOtp }
        }
        AxiosApiHandler.getInstance().resetPasswordRequest(reqBody)
            .then((response) => {
                this.setState({ loading: false })
                this.setState({ isDialogVisible: false });
                if (response.response.status == 202 || response.response.status == 201) {
                    setTimeout(() => Alert.alert("Qurx App", response.response.data.message), 1000)
                    this.props.navigation.navigate("LoginScreen");
                } else {
                    Alert.alert("Error");
                }
            }).catch((error) => {
                var message;
                message = error.error.response.data.message;
                this.setState({ loading: false })
                setTimeout(() => Alert.alert("Qurx App", message), 1000)
            })
    }

    render() {
        return (
            <View style={ResetPasswordStyles.parentViewStyles}>
                <NewAppHeader headerText="Reset Password"
                    isRightActionVisible={true}
                    rightActionText="Back"
                    rightActionCallback={() => {
                        this.backClicked();
                    }}
                    navigation={this.props.navigation} />
                <Loader loading={this.state.loading} />
                <KeyboardAwareScrollView
                    scrollEnabled={this.state.scrollEnabled_for_content}
                    showsVerticalScrollIndicator={false}
                    enableOnAndroid={true}
                    enableAutomaticScroll={true}
                    resetScrollToCoords={{ x: 0, y: 0 }}
                    innerRef={ref => {
                        this.scroll = ref;
                    }}
                    keyboardShouldPersistTaps="handled">
                    <View style={{ margin: 35, }}>
                        {/* <Text style={ResetPasswordStyles.textStyles}>We have sent an OTP to your mobile number, please verify that OTP and generate new password.</Text> */}
                        <Text style={ResetPasswordStyles.textStyles}>{message}</Text>
                        <TextInput maxLength={6} secureTextEntry={true} style={ResetPasswordStyles.resetPassinput}
                            onChangeText={(text) => this.setState({ enteredOtp: text })} placeholder={"Enter OTP"} />
                        <TextInput maxLength={12} secureTextEntry={true} style={ResetPasswordStyles.resetPassinput}
                            onChangeText={(text) => this.setState({ password: text })} placeholder={"Enter new password"} />
                        <TextInput maxLength={12} secureTextEntry={true} style={ResetPasswordStyles.resetPassinput}
                            onChangeText={(text) => this.setState({ confirmPassword: text })} placeholder={"Re-Enter password"} />
                        <TouchableHighlight style={ResetPasswordStyles.submit}
                            onPress={(inputText) => { this.confirmClicked() }}>
                            <View style={ResetPasswordStyles.confirmBtnStyles}>
                                <Text style={ResetPasswordStyles.confirmText}>CONFIRM</Text>
                                <Image style={ResetPasswordStyles.imgStyles} source={arrowBtn} />
                            </View>
                        </TouchableHighlight>
                    </View>
                </KeyboardAwareScrollView>
            </View>
        );
    }
}

export default ResetPassword;
