// import { Button, Container, Content, Text } from "native-base";
// import React, { Component } from "react";
// import { Alert, Animated, Image, Keyboard, View, StatusBar, StyleSheet, TouchableOpacity, TouchableHighlight, Dimensions, TextInput, ScrollView } from "react-native";
// import AppColors from "../../AppColors"
// import ForgotPasswordStyles from "../../styles/ForgotPasswordStyles";
// import NewAppHeader from "../../components/NewAppHeader";
// const arrowBtn = require("../../PagesImages/arrow-button.png");
// import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
// import { RadioGroup, RadioButton } from 'react-native-flexi-radio-button'
// import Dialog, { DialogContent, SlideAnimation } from 'react-native-popup-dialog';
// const closeBtnIcon = require("../../PagesImages/close-button-1.png");
// var deviceHeight = Dimensions.get("window").height;
// var deviceWidth = Dimensions.get("window").width;
// const { width, height } = Dimensions.get("window");
// // Use iPhone6 as base size which is 375 x 667
// const baseWidth = 375;
// const baseHeight = 667;
// new SlideAnimation({
//     initialValue: 0, // optional
//     slideFrom: 'bottom', // optional
//     useNativeDriver: true, // optional
// })
// const scaleWidth = width / baseWidth;
// const scaleHeight = height / baseHeight;
// const scale = Math.min(scaleWidth, scaleHeight);


// export const scaledSize = size => Math.ceil(size * scale);
// export class ForgotPassword extends Component<{}> {

//     constructor(props) {
//         super(props);
//         Text.defaultProps.allowFontScaling = false;
//         this.state = {
//             enteredUserName: "",
//             selectedChoice: "",
//             isDialogVisible: false,
//         };
//     }

//     componentDidMount() {
//         // this.setState({ isDialogVisible: true });
//     }
//     forgotPassClicked() { Alert.alert("Work in progress") }
//     stillNotRecievedOtp() { Alert.alert("Still not recieved OTP clicked ") }
//     backClicked = () => {
//         this.props.navigation.pop();
//     }
//     onSelectChoice() {
//         // selectedGenderVar = value;
//         // this.setState({ selectedGender: value })
//     }

//     renderChoice() {
//         return (
//             <View style={{ marginTop: 10 }}>
//                 <RadioGroup size={20} onSelect={(index, value) => this.onSelectChoice(index, value)}>
//                     <RadioButton value={'SMS'} >
//                         <Text style={ForgotPasswordStyles.greyTextStyles}>SMS</Text>
//                     </RadioButton>

//                     <RadioButton value={'Email'}>
//                         <Text style={ForgotPasswordStyles.greyTextStyles}>Email</Text>
//                     </RadioButton>
//                 </RadioGroup>
//             </View>
//         )
//     }

//     confirmClicked() {
//         this.setState({ isDialogVisible: true });
//     }
//     cancelClicked() {
//         this.setState({ isDialogVisible: false });
//     }
//     confirmChangePassClicked() {
//         Alert.alert("Test");
//     }

//     popUpView() {
//         return (
//             <View style={ForgotPasswordStyles.popUpContainer}>
//                 <Dialog containerStyle={{ marginBottom: 90 }} visible={this.state.isDialogVisible}
//                     dialogAnimation={new SlideAnimation({
//                         slideFrom: 'top',
//                     })}
//                     width={scaledSize(290)}>
//                     <DialogContent>
//                         <TouchableHighlight style={{ position: "absolute", top: 0, right: -2 }}
//                             onPress={() => { this.cancelClicked() }}>
//                             <Image style={ForgotPasswordStyles.cancelImgStyles} source={closeBtnIcon} />
//                         </TouchableHighlight>
//                         <Text style={ForgotPasswordStyles.popUpheadingStyles}>Reset password</Text>
//                         <Text style={ForgotPasswordStyles.textStyles}>We have sent an OTP to your mobile number, please verify that OTP and generate new password.</Text>
//                         <TextInput maxLength={6} style={ForgotPasswordStyles.forgotPassinput} onChangeText={(text) => console.log(text)} placeholder={"Enter OTP"} />
//                         <TextInput maxLength={6} style={ForgotPasswordStyles.forgotPassinput} onChangeText={(text) => console.log(text)} placeholder={"Enter new password"} />
//                         <TextInput maxLength={6} style={ForgotPasswordStyles.forgotPassinput} onChangeText={(text) => console.log(text)} placeholder={"Re-Enter password"} />
//                         <TouchableHighlight style={ForgotPasswordStyles.submit}
//                             onPress={(inputText) => { this.confirmChangePassClicked(this.state.forgotpassEmail) }}>
//                             <View style={ForgotPasswordStyles.confirmBtnStyles}>
//                                 <Text style={ForgotPasswordStyles.confirmText}>CONFIRM</Text>
//                                 <Image style={ForgotPasswordStyles.imgStyles} source={arrowBtn} />
//                             </View>
//                         </TouchableHighlight>
//                     </DialogContent>
//                 </Dialog>
//             </View>
//         );
//     }

//     render() {
//         return (
//             <View style={ForgotPasswordStyles.parentViewStyles}>
//                 <NewAppHeader headerText="Forgot Password"
//                     isRightActionVisible={true}
//                     rightActionText="Back"
//                     rightActionCallback={() => {
//                         this.backClicked();
//                     }}
//                     navigation={this.props.navigation} />
//                 <KeyboardAwareScrollView
//                     scrollEnabled={this.state.scrollEnabled_for_content}
//                     showsVerticalScrollIndicator={false}
//                     enableOnAndroid={true}
//                     enableAutomaticScroll={true}
//                     resetScrollToCoords={{ x: 0, y: 0 }}
//                     innerRef={ref => {
//                         this.scroll = ref;
//                     }}
//                     keyboardShouldPersistTaps="handled">
//                     {this.popUpView()}
//                     <View style={{ margin: 35, }}>
//                         <Text style={ForgotPasswordStyles.headingStyles}>Provide us the mobile number of your Qurx Account and we will send you an email/Otp with instructions to reset your password.</Text>
//                         <TextInput maxLength={6} style={ForgotPasswordStyles.input} onChangeText={(text) => console.log(text)} placeholder={"Enter your registered Mobile number/Email id"} />
//                         {this.renderChoice()}
//                         <TouchableHighlight style={ForgotPasswordStyles.submit}
//                             onPress={(inputText) => { this.confirmClicked(this.state.forgotpassEmail) }}>
//                             <View style={ForgotPasswordStyles.confirmBtnStyles}>
//                                 <Text style={ForgotPasswordStyles.confirmText}>CONFIRM</Text>
//                                 <Image style={ForgotPasswordStyles.imgStyles} source={arrowBtn} />
//                             </View>
//                         </TouchableHighlight>
//                     </View>
//                 </KeyboardAwareScrollView>
//             </View>
//         );
//     }
// }

// export default ForgotPassword;
