import { Text } from "native-base";
import React, { Component } from "react";
import { Alert, Image, View, TouchableOpacity, TouchableHighlight, Dimensions, TextInput } from "react-native";
import OtpScreenStyles from "../../styles/OtpScreenStyles";
import NewAppHeader from "../../components/NewAppHeader";
const arrowBtn = require("../../PagesImages/arrow-button.png");
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
const { width, height } = Dimensions.get("window");
// Use iPhone6 as base size which is 375 x 667
const baseWidth = 375;
const baseHeight = 667;

const scaleWidth = width / baseWidth;
const scaleHeight = height / baseHeight;
const scale = Math.min(scaleWidth, scaleHeight);


export const scaledSize = size => Math.ceil(size * scale);
export class OtpScreen extends Component<{}> {

    constructor(props) {
        super(props);
        Text.defaultProps.allowFontScaling = false;
        this.state = {
            username: "",
        };
    }

    componentDidMount() { }
    forgotPassClicked() { Alert.alert("Forgot pass clicked") }
    stillNotRecievedOtp() { Alert.alert("Still not recieved OTP clicked ") }

    render() {
        return (
            <View style={OtpScreenStyles.parentViewStyles}>
                <NewAppHeader headerText="OTP Confirmation" navigation={this.props.navigation} />
                <KeyboardAwareScrollView
                    scrollEnabled={this.state.scrollEnabled_for_content}
                    showsVerticalScrollIndicator={false}
                    enableOnAndroid={true}
                    enableAutomaticScroll={true}
                    resetScrollToCoords={{ x: 0, y: 0 }}
                    innerRef={ref => {
                        this.scroll = ref;
                    }}
                    keyboardShouldPersistTaps="handled">
                    <View style={{ margin: 35 }}>
                        <Text style={OtpScreenStyles.headingStyles}>We have sent you an OTP on </Text>
                        <Text style={OtpScreenStyles.phoneNoTextStyles}>+918126666641 </Text>
                        <TextInput maxLength={6} style={OtpScreenStyles.input} onChangeText={(text) => console.log(text)} placeholder={"Enter Email Id"} />
                        <View style={OtpScreenStyles.btnContainerStyles}>
                            <TouchableOpacity onPress={() => { this.stillNotRecievedOtp() }}>
                                <Text style={OtpScreenStyles.notRecievedTextStyles}>Still not recieved OTP?</Text>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => { this.forgotPassClicked() }}>
                                <Text style={OtpScreenStyles.resendOtpTextStyles}>Resend OTP</Text>
                            </TouchableOpacity>
                        </View>
                        <TouchableHighlight style={OtpScreenStyles.submit}
                            onPress={() => { this.loginClicked(this.state.forgotpassEmail) }}>
                            <View style={OtpScreenStyles.confirmBtnStyles}>
                                <Text style={OtpScreenStyles.submitText}>CONFIRM BOOKING</Text>
                                <Image style={OtpScreenStyles.imgStyles} source={arrowBtn} />
                            </View>
                        </TouchableHighlight>
                    </View>
                </KeyboardAwareScrollView>
            </View>
        );
    }
}

export default OtpScreen;
