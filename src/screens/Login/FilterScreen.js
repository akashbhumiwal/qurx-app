import { Button, Container, Content, Text } from "native-base";
import React, { Component } from "react";
import { Alert, View, } from "react-native";
import AppColors from "../../themeVariables/AppColors"
import FilterScreenStyles from "../../styles/FilterScreenStyles"
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import CheckBox from '@malik.aliyev.94/react-native-checkbox';
import { RadioGroup, RadioButton } from 'react-native-flexi-radio-button'
import Loader from '../../utility/Loader';
import NewAppHeader from "../../components/NewAppHeader";
import { FlatList, ScrollView, TouchableHighlight } from "react-native-gesture-handler";
var selectedGenderVar = "";
var selectedAvailabilityVar = "";
var isDefaultselected = false;
var isDistanceselected = false;
var isFeeSelected = false;
var isExperienceSelected = false;
var items = [
    { name: 'Default', isSelected: false },
    { name: 'Fee', isSelected: false },
    { name: 'Distance', isSelected: false },
    { name: 'Experience', isSelected: false },
]

export class FilterScreen extends Component<{}> {
    constructor(props) {
        super(props);
        Text.defaultProps.allowFontScaling = false;
        this.state = {
            selectedGender: "",
            selectedAvailability: "",
            selectedCheckboxArray: [],
            checkboxChecked: false,
            loading: false,
            responseArray: [],
        };
        this.onSelectGender = this.onSelectGender.bind(this)
        this.onSelectAvailibility = this.onSelectAvailibility.bind(this)
    }

    componentDidMount() {
        this.setState({ selectedCheckboxArray: [] });
    }


    resetFilterClicked = () => {
        this.setState({ selectedGender: "" });
        this.setState({ selectedAvailability: "" });
        Alert.alert("Reset");
    }

    renderGender() {
        return (
            <View style={{ flex: 1, paddingHorizontal: 20, marginTop: 20, marginBottom: 20 }}>
                <Text style={FilterScreenStyles.blueTextStyles}>Gender</Text>
                <View style={{ flex: 1, paddingHorizontal: 20 }}>
                    <RadioGroup onSelect={(index, value) => this.onSelectGender(index, value)}>
                        <RadioButton value={'Male'} >
                            <Text style={FilterScreenStyles.greyTextStyles}>Male</Text>
                        </RadioButton>
                        <RadioButton value={'Female'}>
                            <Text style={FilterScreenStyles.greyTextStyles}>Female</Text>
                        </RadioButton>
                    </RadioGroup>
                </View>

                <View style={{ marginTop: 10, borderBottomWidth: .7, borderColor: AppColors.gray_medium_light, }} />
            </View>
        )
    }

    renderAvailability() {
        return (
            <View style={{ flex: 1, paddingHorizontal: 20, marginTop: 0 }}>
                <Text style={FilterScreenStyles.blueTextStyles}>Availability</Text>
                <View style={{ flex: 1, paddingHorizontal: 20 }}>
                    <RadioGroup onSelect={(index, value) => this.onSelectAvailibility(index, value)}>
                        <RadioButton value={'This Week'} >
                            <Text style={FilterScreenStyles.greyTextStyles}>This Week</Text>
                        </RadioButton>

                        <RadioButton value={'Today'}>
                            <Text style={FilterScreenStyles.greyTextStyles}>Today</Text>
                        </RadioButton>
                    </RadioGroup>
                </View>
                <View style={{ marginTop: 10, borderBottomWidth: .7, borderColor: AppColors.gray_medium_light, }} />
            </View>
        )
    }


    onChange(input) {
        if (input.name == "Default" && input.checked == true) {
            items[0].isSelected = true;
        } else if (input.name == "Default" && input.checked == false) {
            items[0].isSelected = false;
        }
        else if (input.name == "Distance" && input.checked == true) {
            items[1].isSelected = true;
        } else if (input.name == "Distance" && input.checked == false) {
            items[1].isSelected = false;
        }
        else if (input.name == "Fee" && input.checked == true) {
            items[2].isSelected = true;
        } else if (input.name == "Fee" && input.checked == false) {
            items[2].isSelected = false;
        }
        else if (input.name == "Experience" && input.checked == true) {
            items[3].isSelected = true;
        } else if (input.name == "Experience" && input.checked == false) {
            items[3].isSelected = false;
        }
    }


    renderSortBy() {
        return (
            <View style={{ flex: 1, paddingHorizontal: 20, }}>
                <Text style={FilterScreenStyles.blueTextStyles}>Sort By</Text>
                <View style={{ flex: 1, paddingHorizontal: 20 }}>
                    <CheckBox
                        value={"Default"}
                        name={"Default"}
                        onChange={this.onChange.bind(this)}
                        content='append' style={{ flexDirection: 'row' }}>
                        <Text style={FilterScreenStyles.checkBoxTextStyles}>Default</Text>
                    </CheckBox>

                    <CheckBox
                        value={"Distance"}
                        name={"Distance"}
                        onChange={this.onChange.bind(this)}
                        content='append' style={{ flexDirection: 'row' }}>
                        <Text style={FilterScreenStyles.checkBoxTextStyles}>Distance</Text>
                    </CheckBox>

                    <CheckBox
                        value={"Fee"}
                        name={"Fee"}
                        onChange={this.onChange.bind(this)}
                        content='append' style={{ flexDirection: 'row' }}>
                        <Text style={FilterScreenStyles.checkBoxTextStyles}>Fee</Text>
                    </CheckBox>

                    <CheckBox
                        value={"Experience"}
                        name={"Experience"}
                        onChange={this.onChange.bind(this)}
                        content='append' style={{ flexDirection: 'row' }}>
                        <Text style={FilterScreenStyles.checkBoxTextStyles}>Experience</Text>
                    </CheckBox>
                </View>

            </View>
        )
    }
    onSelectAvailibility(index, value) {
        selectedAvailabilityVar = value;
        this.setState({ selectedAvailability: value })
    }

    onSelectGender(index, value) {
        selectedGenderVar = value;
        this.setState({ selectedGender: value })
    }

    applyClicked() {
        this.props.navigation.navigate('SignUpScreen');
    }

    backClicked = () => {
        this.props.navigation.pop();
    }
    render() {
        const { search } = this.state;
        return (
            <View style={FilterScreenStyles.parentView}>
                <NewAppHeader headerText="Filter"
                    isLeftActionVisible={true}
                    leftActionCallback={() => {
                        this.backClicked();
                    }}
                    isRightActionVisible={true}
                    rightActionText="Reset"
                    rightActionCallback={() => {
                        this.resetFilterClicked();
                    }}
                    navigation={this.props.navigation} />
                <Loader loading={this.state.loading} />
                <KeyboardAwareScrollView
                    scrollEnabled={true}
                    showsVerticalScrollIndicator={false}
                    enableOnAndroid={true}
                    enableAutomaticScroll={true}
                    resetScrollToCoords={{ x: 0, y: 0 }}
                    innerRef={ref => { this.scroll = ref; }}
                    keyboardShouldPersistTaps="handled">
                    {/* Custom Views */}
                    {this.renderGender()}
                    {this.renderAvailability()}
                    {this.renderSortBy()}
                    <TouchableHighlight style={FilterScreenStyles.submit}
                        onPress={(inputText) => { this.applyClicked() }}>
                        <Text style={FilterScreenStyles.submitText}>APPLY</Text>
                    </TouchableHighlight>
                </KeyboardAwareScrollView>
            </View>
        );
    }
}

export default FilterScreen;