import { Text } from "native-base";
import React, { Component } from "react";
import { Alert, View, StyleSheet, Image, Dimensions, FlatList } from "react-native";
import AppColors from "../../themeVariables/AppColors"
import NewAppHeader from "../../components/NewAppHeader";
import LoginScreenStyles from "../../styles/LoginScreenStyles";

import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import { TouchableOpacity } from "react-native-gesture-handler";
const searchIcon = require("../../PagesImages/search-icon.png");
var deviceHeight = Dimensions.get("window").height;
var deviceWidth = Dimensions.get("window").width;
const { width, height } = Dimensions.get("window");
// Use iPhone6 as base size which is 375 x 667           LOGIN MENU
const baseWidth = 375;
const baseHeight = 667;
//LOGIN MENU
const scaleWidth = width / baseWidth;
const scaleHeight = height / baseHeight;
const scale = Math.min(scaleWidth, scaleHeight);
// icon - setting1
const accSettingsImg = require("../../PagesImages/icon-setting1.png");
const profileSettingsImg = require("../../PagesImages/icon-setting2.png");
const blueArrowImg = require("../../PagesImages/right-next.png");

export const scaledSize = size => Math.ceil(size * scale);
export class Settings extends Component<{}> {

    constructor(props) {
        super(props);
        Text.defaultProps.allowFontScaling = false;
        this.state = {
            "settingTypes":
                [{ "name": "Account Settings", "image": accSettingsImg }, { "name": "Profile Settings", "image": profileSettingsImg },]
        };
    }

    componentDidMount() { }

    selectedActionClicled(item) {

        if (item.name == "Account Settings") {
            Alert.alert("Account Settings Work in progress");
            // this.props.navigation.navigate('AccountSettings');
        }
        else if (item.name == "Profile Settings") {
            Alert.alert("Profile Settings Work in progress");
            // this.props.navigation.navigate("ProfileSettings");
        }
    }

    backClicked = () => {
        this.props.navigation.pop();
    }

    render() {
        return (
            <View style={styles.container}>
                <NewAppHeader headerText="Settings"
                    isRightActionVisible={true}
                    rightActionText="Back"
                    rightActionCallback={() => {
                        this.backClicked();
                    }}
                    navigation={this.props.navigation} />
                <KeyboardAwareScrollView
                    scrollEnabled={this.state.scrollEnabled_for_content}
                    showsVerticalScrollIndicator={false}
                    enableOnAndroid={true}
                    enableAutomaticScroll={true}
                    resetScrollToCoords={{ x: 0, y: 0 }}
                    innerRef={ref => {
                        this.scroll = ref;
                    }}
                    keyboardShouldPersistTaps="handled">
                    <View style={{ margin: 20, backgroundColor: "white" }}>
                        <FlatList
                            itemDimension={180}
                            extraData={this.state}
                            data={this.state.settingTypes}
                            renderItem={({ item }) =>
                                <TouchableOpacity style={styles.parentview}
                                    onPress={() => this.selectedActionClicled(item)}>
                                    <View style={styles.flatview}>
                                        <Image style={styles.imgStyles} source={item.image} />
                                        <Text style={styles.name}>{item.name}</Text>
                                    </View>
                                    <Image style={styles.arrowImgStyles} source={blueArrowImg} />
                                </TouchableOpacity>
                            }
                        />
                    </View>
                </KeyboardAwareScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: { flex: 1, justifyContent: "center", justifyContent: "center", backgroundColor: "white" },
    parentview: { flex: 1, flexDirection: "row", alignContent: "center", alignItems: "center", paddingVertical: 20, borderBottomWidth: .25, borderColor: AppColors.theme_text_light_grey_color },
    flatview: { flex: 1, flexDirection: "row", alignContent: "center", alignItems: "center", paddingVertical: 5, },
    name: { fontWeight: "500", paddingHorizontal: 10, fontSize: 20, color: AppColors.theme_text_color, marginHorizontal: 5 },
    imgStyles: { width: 16, height: 20 },
    arrowImgStyles: { marginHorizontal: 10 },
});

export default Settings;

