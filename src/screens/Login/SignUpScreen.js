import { Text } from "native-base";
import React, { Component } from "react";
import { Alert, View, TouchableOpacity, TouchableHighlight, Dimensions, TextInput, Image } from "react-native";
import NewAppHeader from "../../components/NewAppHeader";
import SignUpScreenStyles from "../../styles/SignUpScreenStyles"
import Dialog, { DialogContent } from 'react-native-popup-dialog';
const arrowBtn = require("../../PagesImages/arrow-button.png");
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import AxiosApiHandler from "../../networkHandler/AxiosApiHandler";
import Loader from "../../utility/Loader";
import Utility from "../../utility/Utility";
import StringConstants from "../../utility/StringConstants";
import AsyncStorage from "@react-native-community/async-storage";
const { width, height } = Dimensions.get("window");
// Use iPhone6 as base size which is 375 x 667
const baseWidth = 375;
const baseHeight = 667;
const closeBtnIcon = require("../../PagesImages/close-button-1.png");
const scaleWidth = width / baseWidth;
const scaleHeight = height / baseHeight;
const scale = Math.min(scaleWidth, scaleHeight);
var QruxContext = 0;
var savedUserName = "";
var validatedUserName = "";
export const scaledSize = size => Math.ceil(size * scale);
export class SignUpScreen extends Component<{}> {

    constructor(props) {
        super(props);
        this.state = {
            responsePhoneNum: "",
            isDialogVisible: false,
            qrexContextState: "",
            firstName: "",
            lastName: "",
            email: "",
            mobileNum: "",
            password: "",
            confirmPassword: "",
            loading: false,
            disabled: false,
            devicetype: "",
            devicetoken: "",
            otpTextInput: "",
        };
    }

    componentDidMount() {
        // this.setState({ isDialogVisible: true });
        AsyncStorage.getItem(StringConstants.QREX_ROLE_CONTEXT).then((response) => {
            QruxContext = Number(response);
            this.setState({ qrexContextState: response });

        }).catch((error) => {
            alert(error);
        })
    }

    //1. CREATE OTP REQUEST

    createOtpRequest() {
        // this.setState({ loading: true })
        var reqBody = {
            "dataActionType": "CREATE",
            "userName": savedUserName,
            "qurxRoleContext": QruxContext,
            "content": {}
        }
        AxiosApiHandler.getInstance().createOtp(reqBody)
            .then((response) => {
                // console.log("The api response isssss" + response);
                this.setState({ loading: false })
                if (response.response.status == 201) {
                    this.setState({ isDialogVisible: true });
                } else {
                    Alert.alert("Error");
                }
            }).catch((error) => {
                console.log("error izzz" + JSON.stringify(error));
            })

    }
    //2. VALIDATE OTP REQUEST

    validateOtpRequest() {
        var Otp = Number(this.state.otpTextInput);
        this.setState({ loading: true })
        var reqBody = {
            "userName": savedUserName,
            "dataActionType": "RETRIVE",
            // "qurxRoleContext": QruxContext,
            "content": {
                "otp": Otp
            }
        }
        AxiosApiHandler.getInstance().validateOtp(reqBody)
            .then((response) => {
                this.setState({ loading: false })
                if (response.response.status == 201 || response.response.status == 202) {
                    this.setState({ isDialogVisible: false });
                    setTimeout(() => Alert.alert(response.response.data.message), 2000)
                    setTimeout(() => this.props.navigation.navigate('LoginPage'), 2000)
                } else {
                    this.setState({ loading: false })
                    // this.setState({ isDialogVisible: false });
                    setTimeout(() => Alert.alert("Error" + error.error.response.data.data), 2000)
                    // setTimeout(() => Alert.alert("Error" + error.response.data.data.error[0]), 2000)
                    // Alert.alert("Error");error.response.data.data.error[0]
                }
            }).catch((error) => {
                this.setState({ loading: false })
                // this.setState({ isDialogVisible: false });
                setTimeout(() => Alert.alert(error.error.response.data.data[0]), 2000)
            })

    }

    // SIGN UP REQUEST 

    attemptSignUpRequest() {
        this.setState({ loading: true })
        var reqBody = {
            "qurxRoleContext": 2,
            // "qurxRoleContext": QruxContext,
            "userName": "+91" + this.state.mobileNum,
            "password": this.state.password,
            "firstName": this.state.firstName,
            "lastName": this.state.lastName,
            "email": this.state.email,
        }
        AxiosApiHandler.getInstance().signUp(reqBody)
            .then((response) => {
                this.setState({ loading: false })
                if (response.response.status == 201) {
                    savedUserName = response.response.data.data.userName;
                    this.setState({ isDialogVisible: false });
                    this.createOtpRequest();
                } else {
                    this.setState({ loading: false })
                    Alert.alert("Error");
                }
            }).catch((error) => {
                this.setState({ loading: false })
                setTimeout(() => Alert.alert(error.error.response.data.data.error[0]), 2000)
                console.log("error izzz" + JSON.stringify(error));
            })
    }

    signUpClicked() {
        if (this.validateSignUp() == true) {
            this.attemptSignUpRequest();
        }
    }

    // signUpClicked() {
    //     Alert.alert("Qurx App", "Work in progress");
    //     // if (this.validateSignUp() == true) {
    //     //     this.attemptSignUpRequest();
    //     // }
    // }


    openPopUpClicked() {
        this.setState({ isDialogVisible: true });
    }

    validateSignUp() {
        var message = '';
        let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if (this.state.firstName.replace(/\s/g, '').length == 0) {
            message = StringConstants.ENTER_FIRSTNAME_MESSAGE;
        } else if (this.state.lastName.replace(/\s/g, '').length == 0) {
            message = StringConstants.ENTER_LASTNAME_MESSAGE;
        } else if (reg.test(this.state.email) === false) {
            // message = 'Please enter a valid email'
            message = StringConstants.ENTER_VALID_EMAIL_MESSAGE
        } else if (this.state.mobileNum.replace(/\s/g, '').length == 0 || (this.state.mobileNum.length < 10)) {
            // message = 'Please enter your Mobile number'
            message = StringConstants.ENTER_MOBILENUM_MESSAGE;
        } else if (this.state.password.replace(/\s/g, '').length == 0 || (this.state.password.length < 8)) {
            // message = 'Please enter a valid password a minimum of 8 digits'
            message = StringConstants.ENTER_VALID_DIGITS_PASSWORD_MESSAGE;
        }
        else if (this.state.confirmPassword.replace(/\s/g, '').length == 0) {
            // message = 'Please re enter the password '
            message = StringConstants.REENTER_PASSWORD_MESSAGE;
        }
        else if (this.state.password !== this.state.confirmPassword) {
            // message = 'Passwords does not match !'
            message = StringConstants.PASSWORD_MISMATCH_MESSAGE;
        }
        if (message == '') {
            return true;
        }
        Alert.alert("Qurx App", message);
        return false;
    }

    backClicked = () => {
        this.props.navigation.pop();
    }
    stillNotRecievedOtp() { Alert.alert("Still not recieved OTP clicked ") }
    resendOtpClicked() { Alert.alert("Work in progress"); }
    enterOtpClicked() {
        if (this.state.otpTextInput.length < 6) {
            Alert.alert("Please enter a valid OTP")
        } else {
            this.validateOtpRequest();
        }

    }

    cancelClicked() {
        this.setState({ isDialogVisible: false });
    }

    popUpView() {
        return (
            <View style={SignUpScreenStyles.popUpContainer}>
                <Dialog visible={this.state.isDialogVisible}
                    height={290} width={scaledSize(290)}>
                    <DialogContent>
                        <TouchableHighlight style={{ position: "absolute", top: 0, right: -2 }}
                            onPress={() => { this.cancelClicked() }}>
                            <Image style={SignUpScreenStyles.cancelImgStyles} source={closeBtnIcon} />
                        </TouchableHighlight>
                        <Text style={SignUpScreenStyles.headingStyles}>We have sent you an OTP</Text>
                        <Text style={SignUpScreenStyles.phoneNoTextStyles}>{validatedUserName} </Text>
                        {/* <Text style={SignUpScreenStyles.phoneNoTextStyles}>+918126666641 </Text> */}
                        <TextInput maxLength={6} style={SignUpScreenStyles.input}
                            onChangeText={(text) => this.setState({ otpTextInput: text })}
                            //  onChangeText={(text) => console.log(text)} 
                            placeholder={"Enter OTP"} />
                        <View style={SignUpScreenStyles.btnContainerStyles}>
                            <TouchableOpacity onPress={() => { this.stillNotRecievedOtp() }}>
                                <Text style={SignUpScreenStyles.notRecievedTextStyles}>Still not recieved OTP?</Text>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => { this.resendOtpClicked() }}>
                                <Text style={SignUpScreenStyles.resendOtpTextStyles}>Resend OTP</Text>
                            </TouchableOpacity>
                        </View>
                        <TouchableHighlight style={SignUpScreenStyles.submitOtp}
                            onPress={() => { this.enterOtpClicked() }}>
                            <View style={SignUpScreenStyles.confirmBtnStyles}>
                                <Text style={SignUpScreenStyles.submitOtpText}>SUBMIT</Text>
                                <Image style={SignUpScreenStyles.imgStyles} source={arrowBtn} />
                            </View>
                        </TouchableHighlight>
                    </DialogContent>
                </Dialog>
            </View>
        );
    }

    render() {
        return (
            <View style={SignUpScreenStyles.parentViewStyles}>
                <NewAppHeader
                    isRightActionVisible={true}
                    rightActionText="Back"
                    rightActionCallback={() => {
                        this.backClicked();
                    }}
                    headerText="Sign up" navigation={this.props.navigation} />
                <Loader loading={this.state.loading} />
                <KeyboardAwareScrollView
                    scrollEnabled={this.state.scrollEnabled_for_content}
                    showsVerticalScrollIndicator={false}
                    enableOnAndroid={true}
                    enableAutomaticScroll={true}
                    resetScrollToCoords={{ x: 0, y: 0 }}
                    innerRef={ref => {
                        this.scroll = ref;
                    }}
                    keyboardShouldPersistTaps="handled">
                    {this.popUpView()}
                    <View style={{ marginHorizontal: 20, marginTop: 15 }}>
                        <Text style={SignUpScreenStyles.emailTextStyles}>First Name </Text>
                        <TextInput
                            // keyboardType={'ascii-capable'}
                            style={SignUpScreenStyles.input}
                            onChangeText={(text) => this.setState({ firstName: text })}
                            placeholder={"Enter First Name"} />
                        <Text style={SignUpScreenStyles.emailTextStyles}>Last Name</Text>
                        <TextInput style={SignUpScreenStyles.input}
                            onChangeText={(text) => this.setState({ lastName: text })}
                            placeholder={"Enter Last Name"} />
                        <Text style={SignUpScreenStyles.emailTextStyles}>Email</Text>
                        <TextInput style={SignUpScreenStyles.input}
                            keyboardType={'email-address'}
                            onChangeText={(text) => this.setState({ email: text })}
                            placeholder={"Enter Email"} />
                        <Text style={SignUpScreenStyles.emailTextStyles}>Mobile Number</Text>
                        <TextInput style={SignUpScreenStyles.input}
                            keyboardType="numeric"
                            maxLength={10}
                            onChangeText={(text) => this.setState({ mobileNum: text })}
                            placeholder={"Enter Mobile Number"} />
                        <Text style={SignUpScreenStyles.emailTextStyles}>Password</Text>
                        <TextInput style={SignUpScreenStyles.input}
                            textContentType={'newPassword'}
                            maxLength={12}
                            selectTextOnFocus={true}
                            secureTextEntry={true}
                            onChangeText={(text) => this.setState({ password: text })}
                            placeholder={"Enter Password"} />
                        <Text style={SignUpScreenStyles.emailTextStyles}>Confirm Password</Text>
                        <TextInput style={SignUpScreenStyles.input}
                            textContentType={'newPassword'}
                            selectTextOnFocus={true}
                            maxLength={12}
                            secureTextEntry={true}
                            onChangeText={(text) => this.setState({ confirmPassword: text })}
                            placeholder={"Confirm Password"} />
                        <TouchableHighlight style={SignUpScreenStyles.submit}
                            onPress={(inputText) => { this.signUpClicked(this.state.forgotpassEmail) }}>
                            {/* // onPress={() => { this.setState({ isDialogVisible: true }); }}> */}
                            <Text style={SignUpScreenStyles.submitText}>SIGN UP</Text>
                        </TouchableHighlight>
                    </View>
                </KeyboardAwareScrollView>
            </View>
        );
    }
}


export default SignUpScreen;
