import { Text } from "native-base";
import React, { Component } from "react";
import { Alert, View, StyleSheet, Image, Dimensions, FlatList } from "react-native";
import AppColors from "../../themeVariables/AppColors"
import NewAppHeader from "../../components/NewAppHeader";
import { Share } from 'react-native';
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import { TouchableOpacity } from "react-native-gesture-handler";
const { width, height } = Dimensions.get("window");
// Use iPhone6 as base size which is 375 x 667           LOGIN MENU
const baseWidth = 375;
const baseHeight = 667;
//LOGIN MENU
const scaleWidth = width / baseWidth;
const scaleHeight = height / baseHeight;
const scale = Math.min(scaleWidth, scaleHeight);
const blueArrowImg = require("../../PagesImages/right-next.png");
const loginImg = require("../../PagesImages/login.png");
const signupImg = require("../../PagesImages/signup.png");
const termsImg = require("./../../PagesImages/terms.png");
const shareImg = require("../../PagesImages/share.png");
const followusImg = require("../../PagesImages/followus.png");
const profileSettingsImg = require("../../PagesImages/icon-setting2.png");
export const scaledSize = size => Math.ceil(size * scale);
export class LoginScreen extends Component<{}> {

    constructor(props) {
        super(props);
        Text.defaultProps.allowFontScaling = false;
        this.state = {
            "users":
                [{ "name": "Login", "image": loginImg },
                { "name": "Sign Up", "image": signupImg },
                { "name": "Terms and Policy", "image": termsImg },
                { "name": "Share", "image": shareImg },
                { "name": "Follow Us", "image": followusImg },
                { "name": "Settings", "image": profileSettingsImg },]
        };
    }

    componentDidMount() { }

    selectedActionClicled(item) {

        if (item.name == "Login") {
            this.props.navigation.navigate('LoginPage');
        }
        else if (item.name == "Sign Up") {
            this.props.navigation.navigate("SignUpScreen");
        }
        else if (item.name == "Forgot Password") {
            this.props.navigation.navigate('ForgotPassword');
        }
        else if (item.name == "Terms and Policy") {
            // this.props.navigation.navigate("OtpScreen");
            Alert.alert("Nav to Terms and Policy");
        }
        else if (item.name == "Share") {
            Alert.alert("Nav to Share");
        }
        else if (item.name == "Follow Us") {
            Alert.alert("Nav to Follow Us");
        }
        else if (item.name == "Settings") {
            this.props.navigation.navigate("Settings");
        }
    }

    onClick() {
        Share.share({
            message: 'BAM: we\'re helping your business with awesome React Native apps',
            url: 'http://bam.tech',
            title: 'Wow, did you see that?'
        }, {
            // Android only:
            dialogTitle: 'Share BAM goodness',
            // iOS only:
            excludedActivityTypes: [
                'com.apple.UIKit.activity.PostToTwitter'
            ]
        })
    }

    render() {
        return (
            <View style={styles.container}>
                <NewAppHeader headerText="Login Menu" navigation={this.props.navigation} />
                <KeyboardAwareScrollView
                    scrollEnabled={this.state.scrollEnabled_for_content}
                    showsVerticalScrollIndicator={false}
                    enableOnAndroid={true}
                    enableAutomaticScroll={true}
                    resetScrollToCoords={{ x: 0, y: 0 }}
                    innerRef={ref => {
                        this.scroll = ref;
                    }}
                    keyboardShouldPersistTaps="handled">
                    <View style={{ margin: 20, backgroundColor: "white" }}>
                        <FlatList
                            itemDimension={180}
                            extraData={this.state}
                            data={this.state.users}
                            renderItem={({ item }) =>
                                <TouchableOpacity style={styles.parentview}
                                    onPress={() => this.selectedActionClicled(item)}>
                                    <View style={styles.flatview}>
                                        <Image style={styles.imgStyles} source={item.image} />
                                        <Text style={styles.name}>{item.name}</Text>
                                    </View>
                                    <Image style={styles.arrowImgStyles} source={blueArrowImg} />
                                </TouchableOpacity>
                            }
                        />
                    </View>
                </KeyboardAwareScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: { flex: 1, justifyContent: "center", justifyContent: "center", backgroundColor: "white" },
    parentview: { flex: 1, flexDirection: "row", alignContent: "center", alignItems: "center", paddingVertical: 20, borderBottomWidth: .25, borderColor: AppColors.theme_text_light_grey_color },
    flatview: { flex: 1, flexDirection: "row", alignContent: "center", alignItems: "center", paddingVertical: 5, },
    name: { fontWeight: "500", paddingHorizontal: 10, fontSize: 20, color: AppColors.theme_text_color, marginHorizontal: 5 },
    imgStyles: { width: 20, height: 20 },
    arrowImgStyles: { marginHorizontal: 10 },
});

export default LoginScreen;

