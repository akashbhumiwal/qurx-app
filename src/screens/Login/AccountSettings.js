import { Text } from "native-base";
import React, { Component } from "react";
import { Alert, View, StyleSheet, Dimensions, TextInput, TouchableHighlight } from "react-native";
import AppColors from "../../themeVariables/AppColors"
import NewAppHeader from "../../components/NewAppHeader";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import Applogger from "../../utility/Applogger";
const { width, height } = Dimensions.get("window");
// Use iPhone6 as base size which is 375 x 667           LOGIN MENU
const baseWidth = 375;
const baseHeight = 667;
//LOGIN MENU
const scaleWidth = width / baseWidth;
const scaleHeight = height / baseHeight;
const scale = Math.min(scaleWidth, scaleHeight);
// icon - setting1
const accSettingsImg = require("../../PagesImages/icon-setting1.png");
const profileSettingsImg = require("../../PagesImages/icon-setting2.png");
export const scaledSize = size => Math.ceil(size * scale);
export class AccountSettings extends Component<{}> {

    constructor(props) {
        super(props);
        Text.defaultProps.allowFontScaling = false;
        this.state = {
            "settingTypes":
                [{ "name": "Account Settings", "image": accSettingsImg }, { "name": "Profile Settings", "image": profileSettingsImg },]
        };
    }

    componentDidMount() { }

    backClicked = () => {
        // Applogger.log("nav is");
        // this.props.navigation.navigate('SearchResultsPage');
        this.props.navigation.pop();
    }

    loginClicked() {
        Alert.alert("Account Settings Work in Progress");
    }

    render() {
        return (
            <View style={styles.container}>
                <NewAppHeader headerText="Account Settings"
                    isRightActionVisible={true}
                    rightActionText="Back"
                    rightActionCallback={() => {
                        this.backClicked();
                    }}
                    navigation={this.props.navigation} />
                <KeyboardAwareScrollView
                    scrollEnabled={this.state.scrollEnabled_for_content}
                    showsVerticalScrollIndicator={false}
                    enableOnAndroid={true}
                    enableAutomaticScroll={true}
                    resetScrollToCoords={{ x: 0, y: 0 }}
                    innerRef={ref => {
                        this.scroll = ref;
                    }}
                    keyboardShouldPersistTaps="handled">
                    <View style={{ margin: 35 }}>
                        <Text style={styles.headingStyles}>Change password</Text>
                        <Text style={styles.subHeadingTextStyles}>Current password</Text>
                        <TextInput maxLength={6} style={styles.input} onChangeText={(text) => console.log(text)} placeholder={"Enter your registered email id"} />
                        <Text style={styles.subHeadingTextStyles}>New password</Text>
                        <TextInput maxLength={6} style={styles.input} onChangeText={(text) => console.log(text)} placeholder={"Enter your registered email id"} />
                        <Text style={styles.subHeadingTextStyles}>Confirm New password</Text>
                        <TextInput maxLength={6} style={styles.input} onChangeText={(text) => console.log(text)} placeholder={"Enter your registered email id"} />
                        <TouchableHighlight style={styles.submit}
                            onPress={() => { this.loginClicked(this.state.forgotpassEmail) }}>
                            <Text style={styles.submitText}>LOGIN</Text>
                        </TouchableHighlight>
                    </View>
                </KeyboardAwareScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: { flex: 1, justifyContent: "center", justifyContent: "center" },
    headingStyles: { color: AppColors.theme_text_color, fontSize: 18, fontWeight: 'bold' },
    subHeadingTextStyles: { color: AppColors.theme_text_color, fontSize: 15, marginTop: 20 },
    input: { color: AppColors.theme_text_color, backgroundColor: '#ffffff', fontSize: 15, marginTop: 15, borderWidth: 0.5, height: 60, paddingLeft: 15, paddingRight: 15, borderColor: AppColors.gray_light },
    submit: { marginTop: 28, paddingTop: 5, paddingVertical: 15, backgroundColor: '#1ec97f', borderColor: '#45db5e' },
    submitText: { fontWeight: "600", marginTop: scaledSize(5), fontSize: scaledSize(17), color: '#fff', textAlign: 'center', },
});

export default AccountSettings;

