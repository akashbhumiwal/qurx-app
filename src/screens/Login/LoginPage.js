import { Text } from "native-base";
import React, { Component } from "react";
import { Alert, View, StyleSheet, TouchableOpacity, TouchableHighlight, Dimensions, TextInput, Image } from "react-native";
import AppColors from "../../themeVariables/AppColors"
import NewAppHeader from "../../components/NewAppHeader";
import LoginScreenStyles from "../../styles/LoginScreenStyles";
import AxiosApiHandler from "../../networkHandler/AxiosApiHandler";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import Dialog, { DialogContent, SlideAnimation } from 'react-native-popup-dialog';
import { RadioGroup, RadioButton } from 'react-native-flexi-radio-button'
import AsyncStorage from "@react-native-community/async-storage";
import StringConstants from "../../utility/StringConstants";
import Utility from "../../utility/Utility";
import Loader from "../../utility/Loader";
const { width, height } = Dimensions.get("window");
import UserContext from '../../utility/UserContext';
const arrowBtn = require("../../PagesImages/arrow-button.png");
const closeBtnIcon = require("../../PagesImages/close-button-1.png");
// Use iPhone6 as base size which is 375 x 667
const baseWidth = 375;
const baseHeight = 667;
const scaleWidth = width / baseWidth;
const scaleHeight = height / baseHeight;
const scale = Math.min(scaleWidth, scaleHeight);
var QruxContext = 0;
var UserData = {};
var selectedOption = "";
export const scaledSize = size => Math.ceil(size * scale);
export class LoginPage extends Component<{}> {
    static contextType = UserContext
    constructor(props) {
        super(props);
        Text.defaultProps.allowFontScaling = false;
        this.state = {
            isDialogVisible: false,
            loading: false,
            scrollEnabled_for_content: false,
            password_input: "",
            username_input: "",
            qrexContextState: "",
            selectedPopupChoice: "",
            enterednumberOrmail: "",
        };
        this.onSelectChoice = this.onSelectChoice.bind(this)
    }

    componentDidMount() {
        const user = this.context
        console.log(user);
        // { name: 'Tania', loggedIn: true }
        // AsyncStorage.getItem(StringConstants.QREX_ROLE_CONTEXT).then((response) => {
        //     QruxContext = Number(response);
        //     this.setState({ qrexContextState: response });

        // }).catch((error) => {
        //     alert(error);
        // })
    }
    //////////////////POP UP FUNCTIONALITY/////////////////
    validatePopUp() {
        var message = '';
        let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if (this.state.enterednumberOrmail.replace(/\s/g, '').length == 0) {
            message = StringConstants.ENTER_USERNAME_MESSAGE;
        } else if (selectedOption.replace(/\s/g, '').length == 0) {
            message = StringConstants.SELECT_CHOICE_MESSAGE;
        }
        if (message == '') {
            return true;
        }
        Alert.alert("Qurx App", message);
        return false;
    }


    popUpconfirmClicked() {
        if (this.validatePopUp() == true) {
            this.confirmCreateLink();
        }
    }

    confirmCreateLink() {
        QruxContext = 2;
        this.setState({ loading: true })
        var reqBody = {
            "dataActionType": "CREATE",
            "userName": "+91" + this.state.enterednumberOrmail,
            "qurxRoleContext": QruxContext,
            "content": { "passwordResetMethod": selectedOption }
        }
        var usernameTosend = reqBody.userName;
        AxiosApiHandler.getInstance().createUpdateLinkRequest(reqBody)
            .then((response) => {
                this.setState({ loading: false })
                this.setState({ isDialogVisible: false });
                if (response.response.status == 200 || response.response.status == 201) {
                    UserData = response.response.data.data;
                    this.props.navigation.navigate('ResetPassword', { usernameTosend, selectedOption });
                } else {
                    Alert.alert("Qurx App", "Error");
                }
            }).catch((error) => {
                var message;
                message = error.error.response.data.data[0];
                this.setState({ loading: false })
                setTimeout(() => Alert.alert("Qurx App", message), 2000)

            })
    }

    cancelClicked() {
        this.setState({ isDialogVisible: false });
    }
    onSelectChoice(index, value) {
        selectedOption = value;
    }
    //////////////////////////////////////////////////////////////////////////////////////////


    forgotPassClicked() {
        this.setState({ isDialogVisible: true });
    }


    validateLogin() {
        var message = '';
        let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if (this.state.username_input.replace(/\s/g, '').length == 0) {
            message = StringConstants.ENTER_USERNAME_MESSAGE;
        } else if (this.state.password_input.replace(/\s/g, '').length == 0) {
            message = StringConstants.ENTER_VALID_PASSWORD_MESSAGE;
        }
        if (message == '') {
            return true;
        }
        Alert.alert(StringConstants.QURX_APP_ALERT, message);
        return false;
    }

    loginClicked() {
        if (this.validateLogin() == true) {
            this.attemptLoginAuthRequest();
        }
    }


    attemptLoginAuthRequest() {
        ////For Demo only QruxContext = 1;
        // QruxContext = 1;
        QruxContext = 2;
        this.setState({ loading: true })
        var reqBody = {
            "qurxRoleContext": QruxContext,
            "userName": "+91" + this.state.username_input,
            "password": this.state.password_input,
        }
        AxiosApiHandler.getInstance().loginAuthenticate(reqBody)
            .then((response) => {
                // console.log("The api response isssss" + response);
                this.setState({ loading: false })
                if (response.response.status == 200) {
                    UserData = response.response.data.data;
                    Utility.getInstance().storeLoginUserData(StringConstants.LOGIN_USER_DATA, JSON.stringify(UserData));
                    setTimeout(() => Alert.alert(StringConstants.LOGIN_SUCCESSFUL_MESSAGE), 2000)
                } else {
                    Alert.alert(StringConstants.ERROR_MESSAGE);
                }
            }).catch((error) => {
                var message;
                if (error.error.response.data.data != "" || error.error.response.data.data != null) {
                    message = error.error.response.data.data;
                } else {
                    message = error.error.response.data.data[0];
                }
                this.setState({ loading: false })
                setTimeout(() => Alert.alert(message), 2000)
            })
    }


    backClicked = () => {
        this.props.navigation.pop();
    }

    renderChoice() {
        return (
            <View style={{ marginTop: 10, marginBottom: 5 }}>
                <RadioGroup size={15} onSelect={(index, value) => this.onSelectChoice(index, value)}>
                    <RadioButton value={'sms'} >
                        <Text style={LoginScreenStyles.greyTextStyles}>SMS</Text>
                    </RadioButton>

                    <RadioButton value={'email'}>
                        <Text style={LoginScreenStyles.greyTextStyles}>Email</Text>
                    </RadioButton>
                </RadioGroup>
            </View>
        )
    }


    popUpView() {
        return (
            <View style={LoginScreenStyles.popUpContainer}>
                <Dialog containerStyle={{ marginTop: -100 }} visible={this.state.isDialogVisible}
                    dialogAnimation={new SlideAnimation({
                        slideFrom: 'top',
                    })}
                    width={scaledSize(290)}>
                    <DialogContent>
                        <TouchableHighlight style={{ position: "absolute", top: 0, right: -2 }}
                            onPress={() => { this.cancelClicked() }}>
                            <Image style={LoginScreenStyles.cancelImgStyles} source={closeBtnIcon} />
                        </TouchableHighlight>

                        {/* "Forgot Password" */}
                        <View style={{ marginTop: 15, justifyContent: "center" }}>
                            <Text style={LoginScreenStyles.popUpheadingStyles}>Forgot Password</Text>
                            <Text style={LoginScreenStyles.headingStyles}>Provide us the mobile number of your Qurx Account and we will send you an email/Otp with instructions to reset your password.</Text>
                            <TextInput maxLength={10} style={LoginScreenStyles.popUpinput}
                                onChangeText={(text) => this.setState({ enterednumberOrmail: text })}
                                placeholder={"Enter your registered Mobile number/Email id"} />
                            {this.renderChoice()}
                            <TouchableHighlight style={LoginScreenStyles.popUpsubmit}
                                onPress={(inputText) => { this.popUpconfirmClicked(this.state.forgotpassEmail) }}>
                                {/* <View style={LoginScreenStyles.popUpconfirmBtnStyles}> */}
                                <Text style={LoginScreenStyles.confirmText}>CONFIRM</Text>
                                {/* </View> */}
                            </TouchableHighlight>
                        </View>
                    </DialogContent>
                </Dialog>
            </View>
        );
    }

    render() {
        return (
            <View style={LoginScreenStyles.container}>
                <NewAppHeader
                    isRightActionVisible={true}
                    rightActionText="Back"
                    rightActionCallback={() => {
                        this.backClicked();
                    }}
                    headerText="Login" navigation={this.props.navigation} />
                <Loader loading={this.state.loading} />
                <KeyboardAwareScrollView
                    scrollEnabled={this.state.scrollEnabled_for_content}
                    showsVerticalScrollIndicator={false}
                    enableOnAndroid={true}
                    enableAutomaticScroll={true}
                    resetScrollToCoords={{ x: 0, y: 0 }}
                    innerRef={ref => { this.scroll = ref; }}
                    keyboardShouldPersistTaps="handled">
                    {this.popUpView()}
                    <View style={{ margin: 20 }}>
                        <Text style={LoginScreenStyles.emailTextStyles}>Phone Number</Text>
                        <TextInput style={LoginScreenStyles.input} keyboardType="numeric" maxLength={10} placeholder={"Enter Phone Number"}
                            onChangeText={(text) => this.setState({ username_input: text })} />
                        <Text style={LoginScreenStyles.emailTextStyles}>Password</Text>
                        <TextInput style={LoginScreenStyles.input} maxLength={12} secureTextEntry={true} placeholder={"Enter Password"}
                            onChangeText={(text) => this.setState({ password_input: text })} />
                        <TouchableOpacity onPress={(inputText) => { this.forgotPassClicked() }}>
                            <Text style={{ color: AppColors.theme_text_color, fontSize: 17, marginTop: 40, fontWeight: "600" }}>Forgot Password?</Text>
                        </TouchableOpacity>
                        <TouchableHighlight style={LoginScreenStyles.submit}
                            onPress={(inputText) => { this.loginClicked(this.state.forgotpassEmail) }}>
                            <Text style={LoginScreenStyles.submitText}>LOGIN</Text>
                        </TouchableHighlight>
                    </View>
                </KeyboardAwareScrollView>
            </View>
        );
    }
}

export default LoginPage;

