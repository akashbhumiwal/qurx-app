import { Button, Container, Content, Text } from "native-base";
import React, { Component } from "react";
import { Alert, Animated, Image, Keyboard, View, StatusBar, StyleSheet, TouchableOpacity, TouchableHighlight, Dimensions, TextInput } from "react-native";
import AppColors from "../../themeVariables/AppColors"
import ProfileSettingsStyles from "../../styles/ProfileSettingsStyles";
import ImagePicker from 'react-native-image-picker';
import NewAppHeader from "../../components/NewAppHeader";
const arrowBtn = require("../../PagesImages/arrow-button.png");
const camBtn = require("../../PagesImages/icon-camera.png");
const dummyProfileImage = require("../../PagesImages/profile-dummy-pic.png");
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
var deviceHeight = Dimensions.get("window").height;
var deviceWidth = Dimensions.get("window").width;
const { width, height } = Dimensions.get("window");
// Use iPhone6 as base size which is 375 x 667
const baseWidth = 375;
const baseHeight = 667;

const scaleWidth = width / baseWidth;
const scaleHeight = height / baseHeight;
const scale = Math.min(scaleWidth, scaleHeight);


export const scaledSize = size => Math.ceil(size * scale);
export class ProfileSettings extends Component<{}> {

    constructor(props) {
        super(props);
        Text.defaultProps.allowFontScaling = false;
        this.state = {
            username: "",
        };
    }

    componentDidMount() { }
    forgotPassClicked() { Alert.alert("Work in progress") }
    stillNotRecievedOtp() { Alert.alert("Still not recieved OTP clicked ") }
    backClicked = () => {
        this.props.navigation.pop();
    }



    selectPhotoTapped() {

        console.log("Calling camera");
        const options = {
            quality: 1.0,
            maxWidth: 500,
            maxHeight: 500,
            storageOptions: {
                path: 'images',
            },
        };

        ImagePicker.launchCamera(options, (response) => {
            console.log('Response = ', response);

            if (response.didCancel) {
                console.log('User cancelled photo picker');
            }
            else if (response.error) {
                console.log('User clicked cam');
                console.log('ImagePicker Error: ', response.error);
            }
            else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            }
            else {
                const source = { uri: response.uri };
                console.log("Akash" + JSON.stringify(source));
                this.setState({ pictureBase64: response.uri });

                console.log("HELLO", response.uri);
                console.log("Image issssss" + source.uri);

                this.setState({
                    camClickedImage: source

                });
            }
        });
    }

    render() {
        return (
            <View style={ProfileSettingsStyles.parentViewStyles}>
                <NewAppHeader headerText="Profile Settings"
                    isRightActionVisible={true}
                    rightActionText="Back"
                    rightActionCallback={() => {
                        this.backClicked();
                    }}
                    navigation={this.props.navigation} />
                <KeyboardAwareScrollView
                    scrollEnabled={this.state.scrollEnabled_for_content}
                    showsVerticalScrollIndicator={false}
                    enableOnAndroid={true}
                    enableAutomaticScroll={true}
                    resetScrollToCoords={{ x: 0, y: 0 }}
                    innerRef={ref => {
                        this.scroll = ref;
                    }}
                    keyboardShouldPersistTaps="handled">
                    <View>
                        <View style={{ flex: 1, flexDirection: "column", alignItems: "center", }}>
                            <View style={{ width: "100%", height: 70, backgroundColor: AppColors.gray_mid_light }} />
                            <View style={{ width: "100%", height: 70, }} />
                            <TouchableOpacity onPress={this.selectPhotoTapped.bind(this)}>
                                <Image style={ProfileSettingsStyles.profileDummyImageStyles} source={dummyProfileImage} />
                                <Image style={ProfileSettingsStyles.camImgStyles} source={camBtn} />
                            </TouchableOpacity>
                        </View>
                        <View style={{ marginHorizontal: 35, }}>
                            <Text style={ProfileSettingsStyles.headingStyles}>Full Name</Text>
                            <TextInput style={ProfileSettingsStyles.input} onChangeText={(text) => console.log(text)} placeholder={"Enter your Full Name"} />
                            <Text style={ProfileSettingsStyles.headingStyles}>Mobile</Text>
                            <TextInput style={ProfileSettingsStyles.input} onChangeText={(text) => console.log(text)} placeholder={"Enter your Mobile"} />
                            <Text style={ProfileSettingsStyles.headingStyles}>Email</Text>
                            <TextInput style={ProfileSettingsStyles.input} onChangeText={(text) => console.log(text)} placeholder={"Enter your registered email id"} />
                            <TouchableHighlight style={ProfileSettingsStyles.submit}
                                onPress={(inputText) => { this.forgotPassClicked(this.state.forgotpassEmail) }}>
                                <View style={ProfileSettingsStyles.confirmBtnStyles}>
                                    <Text style={ProfileSettingsStyles.confirmText}>CONFIRM</Text>
                                    <Image style={ProfileSettingsStyles.imgStyles} source={arrowBtn} />
                                </View>
                            </TouchableHighlight>
                        </View>
                    </View>
                </KeyboardAwareScrollView>
            </View>
        );
    }
}

export default ProfileSettings;
