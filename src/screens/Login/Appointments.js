import { Text } from "native-base";
import React, { Component } from "react";
import { Alert, View, StyleSheet, Dimensions } from "react-native";
import AppColors from "../../themeVariables/AppColors"
import AxiosApiHandler from "../../networkHandler/AxiosApiHandler";
import NewAppHeader from "../../components/NewAppHeader";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";

export class Appointments extends Component<{}> {

    constructor(props) {
        super(props);
        Text.defaultProps.allowFontScaling = false;
        this.state = {
            username_input: false
        };
    }

    componentDidUpdate(prevProps) {
    }
    componentDidMount() {
        // this.getSettingDetails();
    }

    getSettingDetails() {
        const api = AxiosApiHandler.getInstance();
        api.getSpecialityList().then((response) => {
            if (response) {
                responseArray = response.data;
            } else {
                Alert.alert("Error !");
            }
        });
    }

    render() {
        return (
            <View style={{ justifyContent: "center", justifyContent: "center" }}>
                <NewAppHeader headerText="My Appointments" navigation={this.props.navigation} />
                <KeyboardAwareScrollView
                    scrollEnabled={this.state.scrollEnabled_for_content}
                    showsVerticalScrollIndicator={false}
                    enableOnAndroid={true}
                    enableAutomaticScroll={true}
                    resetScrollToCoords={{ x: 0, y: 0 }}
                    innerRef={ref => {
                        this.scroll = ref;
                    }}
                    keyboardShouldPersistTaps="handled">
                </KeyboardAwareScrollView>
                <View style={{ marginTop: 50, alignSelf: "center" }}>
                    <Text style={{ marginTop: 50, fontSize: 30, color: AppColors.theme_text_color }}>WORK IN PROGRESS</Text>
                </View>
            </View>
        );
    }
}

export default Appointments;

