
import AppConstants from "../utility/AppConstants";
var axios = require("axios");
var authToken = null;
export default class AxiosApiHandler {
  instance = null;
  // reservationLimit = 20; // number of reservations to be fetched from api

  /**
   * provides token for api calls
   * @param {token for api call} authToken
   */
  static getInstance(token) {
    if (this.instance == null) {
      this.instance = new AxiosApiHandler();
    }
    // authToken = token + "1";
    authToken = token;
    return this.instance;
  }

  /**
   * returns header
   */
  getHeader(isMultipart = false) {
    return {
      headers:
        authToken != null && authToken != undefined
          ? this.getHeaderWithToken(isMultipart)
          : this.getHeaderWithoutToken()
    };
  }

  /**
   * returns header with authtoken
   */
  getHeaderWithToken(isMultipart = false) {
    if (isMultipart) {
      return {
        Authorization: "Bearer " + authToken,
        Referer: AppConstants.BASE_URL,
        "Content-Type": "multipart/form-data"
      };
    }
    return {
      Authorization: "Bearer " + authToken,
      Referer: AppConstants.BASE_URL
    };
  }

  /**
   * returns header without token
   */
  getHeaderWithoutToken() {
    return {
      Referer: AppConstants.Referer_URL
    };
  }
  /**
   * Logs user in to the app.
   * @param {users's email address} email
   * @param {user's password} password
   */


  // API REQUESTS //

  // SETTINGS/ROLES REQUEST

  getSpecialityList(reqBody) {
    return new Promise((resolve, reject) => {
      axios.post(AppConstants.BASE_URL + "/api/setting", reqBody)
        // axios.post("https://api.qurx.in/api/setting", reqBody)
        .then(function (response) {
          return resolve({ response });
        })
        .catch(function (error) {
          return reject({ error });
        });
    });
  }

  // SIGNUP REQUEST

  signUp(reqBody) {
    return new Promise((resolve, reject) => {
      axios.post(AppConstants.BASE_URL + "/api/signup", reqBody)
        // axios.post("https://api.qurx.in/api/signup", reqBody)
        .then(function (response) {
          return resolve({ response });
        })
        .catch(function (error) {
          return reject({ error });
        });
    });
  }


  // LOGIN REQUEST

  loginAuthenticate(reqBody) {
    return new Promise((resolve, reject) => {
      axios.post(AppConstants.BASE_URL + "/api/authenticate", reqBody)
        // axios.post("https://api.qurx.in/api/authenticate", reqBody)
        .then(function (response) {
          return resolve({ response });
        })
        .catch(function (error) {
          return reject({ error });
        });
    });
  }

  // CREATE OTP REQUEST

  createOtp(reqBody) {
    return new Promise((resolve, reject) => {
      // axios.post(AppConstants.BASE_URL + "/api/signup", reqBody)
      axios.post("https://api.qurx.in/api/otp", reqBody)
        .then(function (response) {
          return resolve({ response });
        })
        .catch(function (error) {
          return reject({ error });
        });
    });
  }

  // VALIDATE OTP REQUEST

  validateOtp(reqBody) {
    return new Promise((resolve, reject) => {
      // axios.post(AppConstants.BASE_URL + "/api/otp", reqBody)
      axios.post("https://api.qurx.in/api/otp", reqBody)
        .then(function (response) {
          return resolve({ response });
        })
        .catch(function (error) {
          return reject({ error });
        });
    });
  }

  // CREATE UPDATE LINK REQUEST(FORGOT PASSWORD)

  createUpdateLinkRequest(reqBody) {
    return new Promise((resolve, reject) => {
      axios.post(AppConstants.BASE_URL + "/api/passwordReset", reqBody)
        // axios.post("https://api.qurx.in/api/passwordReset", reqBody)
        .then(function (response) {
          return resolve({ response });
        })
        .catch(function (error) {
          return reject({ error });
        });
    });
  }


  // PASSWORD UPDATE REQUEST(RESET PASSWORD)

  resetPasswordRequest(reqBody) {
    return new Promise((resolve, reject) => {
      axios.post(AppConstants.BASE_URL + "/api/passwordReset", reqBody)
        .then(function (response) {
          return resolve({ response });
        })
        .catch(function (error) {
          return reject({ error });
        });
    });
  }

  // SEARCH DOCTORS REQUEST

  searchDoctors(reqBody) {
    return new Promise((resolve, reject) => {
      axios.post(AppConstants.BASE_URL + "/api/search/doctor", reqBody)
        .then(function (response) {
          return resolve({ response });
        })
        .catch(function (error) {
          return reject({ error });
        });
    });
  }
}
