
import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
const Stack = createStackNavigator();
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
const Tab = createMaterialBottomTabNavigator();
import AppNavigator from "./src/components/AppNavigator";

const App: () => React$Node = () => {
  const user = { name: 'Tania', loggedIn: true }
  return (
    <AppNavigator></AppNavigator>
  );
};

export default App;
